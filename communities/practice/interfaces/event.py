# -*- coding: utf-8 -*-
from zope.component.interfaces import IObjectEvent
from zope.interface import Attribute

class ICoPLocalRolesChangedEvent(IObjectEvent):
    """
        Evento disparado apos a alteracao de local roles na CoP
    """
    member_id = Attribute("The member id")
    role = Attribute("The role")

class ICoPMasterRolesChangedEvent(IObjectEvent):
    """
        Evento disparado apos a alteracao de master roles na CoPCommunityFolder
    """
    member_id = Attribute("The member id")
    role = Attribute("The role")

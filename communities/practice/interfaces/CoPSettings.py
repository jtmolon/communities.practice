# -*- coding: utf-8 -*-
from zope import schema
from zope.interface import Interface

from communities.practice import practiceMessageFactory as _
from communities.practice.generics.vocabularies import generateVocabulary
from communities.practice.generics.vocabularies import\
    AVAILABLE_FORMS_VOCABULARY


class ICoPSettings(Interface):
    """ Marker base interface for cop settings
    """
    hostname = schema.TextLine(
        title=_(u'label_hostname', default=u'Host name'),
        description=_(u'help_hostname', u'Endereço do servidor SMTP'),
        required=True,
        default=u'smtp.gmail.com',
    )
    port = schema.Int(
        title=_(u'label_port', default=u'Porta'),
        description=_(u'help_port', u'Porta SMTP'),
        required=True,
        default=587,
    )
    username = schema.TextLine(
        title=_(u'label_username', default=u'Endereço de e-mail'),
        description=_(u'help_username', u'Endereço de e-mail'),
        required=True,
        default=u'testeplone@gmail.com',
    )
    password = schema.Password(
        title=_(u'label_password', default=u'Senha'),
        description=_(u'help_password', u'Senha do endereço de e-mail'),
        required=False,
        default=u'testeplone1234',
    )
    force_tls = schema.Bool(
        title=_(u'label_force_tls', default=u'Force TLS'),
        description=_('help_force_tls', u'Force TLS'),
        required=False,
        default=False,
    )
    available_forms = schema.Set(
        title=_(u'label_available_forms', default=u'Available Forms'),
        description=_(
            'help_available_forms',
            u'Choose the available forms form cop.forms product',
        ),
        required=False,
        value_type=schema.Choice(
            vocabulary=generateVocabulary(AVAILABLE_FORMS_VOCABULARY),
        ),
        default=set(),
    )

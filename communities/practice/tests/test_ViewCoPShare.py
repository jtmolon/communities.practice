# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase


class ViewCoPShareTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPShareTestCase, self).setUp()
        self.factoryCoPShare()
        self.view = self.getView(self.copshare, 'viewCoPShare')

    def test_get_parent_url(self):
        parent_url = '%s/view' % self.copdocument.absolute_url()
        self.assertEqual(self.view.get_parent_url(), parent_url)

# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase

class ViewsRegisteredTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewsRegisteredTestCase, self).setUp()
        self.factorySubCoP()
        self.factoryCoPDocument()
        self.factoryCoPPortfolio()
        self.factoryCoPUpload()
        self.factoryCoPLink()
        self.factoryCoPShare()
        self.factoryCoPImage()
        self.factoryCoPFile()
        self.factoryCoPATA()
        self.factoryCoPPost()
        self.factoryCoPEvent()

    def test_views_registered(self):
        views = [
            (self.cop, 'viewCoP'),
            (self.subcopmenu, 'viewCoPSubCoPMenu'),
            (self.copdocument, 'viewCoPComment'),
            (self.copmenu, 'viewCoPNotification'),
            (self.copmenu, 'viewCoPMenu'),
            (self.copcommunityfolder, 'viewCoPCommunityFolder'),
            (self.cop, 'viewCoPLeave'),
            (self.copcommunityfolder, 'viewCoPCommunityFolderActivity'),
            (self.copcommunityfolder, 'viewCoPMembersNeverLogged'),
            (self.copmenu, 'viewCoPMenuPortfolio'),
            (self.copfolder, 'viewCoPFolder'),
            (self.cop, 'viewCoPSearch'),
            (self.copcommunityfolder, 'viewCoPCommunityFolderConfig'),
            (self.copmenu, 'viewCoPActivity'),
            (self.copmenu, 'viewCoPConfig'),
            (self.copmenu, 'viewCoPActivityContent'),
            (self.cop, 'viewCoPBaseBanner'),
            (self.cop, 'viewCoPBaseMenu'),
            (self.cop, 'viewCoPTimeline'),
            (self.copportfolio, 'viewCoPPortfolio'),
            (self.copupload, 'viewCoPUpload'),
            (self.coplink, 'viewCoPLink'),
            (self.copshare, 'viewCoPShare'),
            (self.copimage, 'viewCoPImage'),
            (self.copevent, 'viewCoPEvent'),
            (self.copdocument, 'viewCoPDocument'),
            (self.copfile, 'viewCoPFile'),
            (self.cop, 'viewCoPInvitation'),
            (self.copata, 'viewCoPATA'),
            (self.coppost, 'viewCoPPost'),
            (self.cop, 'viewCoPMembers'),
            (self.copmenu, 'viewCoPFolderContents'),
            (self.cop, 'viewCoPCustomEdit'),
        ]
        for context, view_name in views:
            self.assertIsNotNone(
                self.getView(context, view_name),
                msg='Unregistered view: %s' % view_name
            )

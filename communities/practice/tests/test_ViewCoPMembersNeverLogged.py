# -*- coding: utf-8 -*-
from DateTime import DateTime
from communities.practice.tests.CoPBase import IntegrationTestCase
from plone.app.testing import TEST_USER_ID


class ViewCoPMembersNeverLoggedTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPMembersNeverLoggedTestCase, self).setUp()
        self.factoryCoPCommunityFolder()
        self.view = self.getView(
            self.copcommunityfolder, 'viewCoPMembersNeverLogged'
        )

    def test_set_cop_menu_selected(self):
        self.assertEqual(
            self.view.set_cop_menu_selected(), 'copcommunityneverlogged'
        )

    def test_set_context_title(self):
        context_title = self.view.set_context_title()
        self.assertEqual(
            context_title['title'], 'Usuários que nunca fizeram login'
        )

    def test_send_message_members(self):
        self.assertFalse(self.view.send_message_members())
        member = self.portal.portal_membership.getAuthenticatedMember()
        self.setMembersData([member.getId()])
        context = self.view.context
        self.view.request.form = {
            'submit_send_message_members': 'enviar'
        }
        self.view.send_message_members()
        self.request.response.setStatus('400')
        messages = context.plone_utils.showPortalMessages()
        self.assertEqual(
            messages[0].message, u'Campos obrigatórios não preenchidos'
        )
        self.view.request.form['assunto'] = 'assunto'
        self.view.request.form['mensagem'] = 'mensagem'
        self.view.send_message_members()
        self.request.response.setStatus('400')
        messages = context.plone_utils.showPortalMessages()
        self.assertEqual(
            messages[0].message, u'Selecione emails para envio.'
        )
        self.view.request.form[TEST_USER_ID] = 'Sim'
        self.view.send_message_members()
        self.request.response.setStatus('400')
        messages = context.plone_utils.showPortalMessages()
        self.assertEqual(
            messages[0].message, u'Email enviado com sucesso.'
        )

    def test_get_members(self):
        member = self.portal.portal_membership.getAuthenticatedMember()
        self.assertEqual(len(self.view.get_members()), 1)
        member.setMemberProperties(
            mapping={'last_login_time': DateTime('2015/01/01 00:00:00 GMT-2')}
        )
        self.assertEqual(len(self.view.get_members()), 0)

    def test_get_action_url(self):
        self.assertEqual(
            self.view.get_action_url(),
            '%s/viewCoPMembersNeverLogged' % self.view.context.absolute_url()
        )

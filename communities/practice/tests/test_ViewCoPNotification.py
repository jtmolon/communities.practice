# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.generics.generics import setMasterRole
from plone.app.testing import TEST_USER_ID


class ViewCoPNotificationTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPNotificationTestCase, self).setUp()
        self.factoryCoPMenu()
        self.view = self.getView(self.copmenu, 'viewCoPNotification')

    def test_send_message_members(self):
        self.assertFalse(self.view.send_message_members())
        member = self.portal.portal_membership.getAuthenticatedMember()
        self.setMembersData([member.getId()])
        context = self.view.context
        self.request.form = {
            'submit_send_message_members': 'Enviar'
        }
        self.view.send_message_members()
        self.request.response.setStatus('400')
        messages = context.plone_utils.showPortalMessages()
        self.assertEqual(
            messages[0].message, u'Campos obrigatórios não preenchidos'
        )
        self.view.request.form['assunto'] = 'assunto'
        self.view.request.form['mensagem'] = 'mensagem'
        self.view.send_message_members()
        self.request.response.setStatus('400')
        messages = context.plone_utils.showPortalMessages()
        self.assertEqual(
            messages[0].message, u'Selecione emails para envio.'
        )
        self.view.request.form[TEST_USER_ID] = 'Sim'
        self.view.send_message_members()
        self.request.response.setStatus('400')
        messages = context.plone_utils.showPortalMessages()
        self.assertEqual(
            messages[0].message, u'Email enviado com sucesso.'
        )

    def test_get_members(self):
        participantes = self.view.get_members()
        self.assertEqual(len(participantes), 0)
        self.addUsersCoP()
        participantes = self.view.get_members()
        self.assertEqual(len(participantes), 3)
        resultado = [
            {'id': TEST_USER_ID, 'nome': TEST_USER_ID},
            {'id': 'user_moderador', 'nome': 'user_moderador'},
            {'id': 'user_participante', 'nome': 'user_participante'}
        ]
        self.assertEqual(participantes, resultado)

    def test_store_notification(self):
        self.view.store_notification(
            [], ['test@test.com'], 'subject', 'message'
        )
        self.assertIn('message', self.view.context)
        self.assertIn('test@test.com', self.view.context.message.getText())

    def test_get_action_url(self):
        self.assertEqual(
            self.view.get_action_url(), self.view.context.absolute_url()
        )

    def addUsersCoP(self):
        users_ids = [
            'user_participante',
            'user_moderador',
            'user_moderador_master',
        ]
        self.createUsers(users_ids)
        users_ids.append(
            self.portal.portal_membership.getAuthenticatedMember().getId()
        )
        self.setMembersData(users_ids)
        self.setRoleInCoP(self.cop, 'user_participante', 'Participante')
        self.setRoleInCoP(self.cop, 'user_moderador', 'Moderador')
        setMasterRole(self.cop, 'user_moderador_master', 'Moderador_Master')
        self.createUsers(['user_sem_email'])
        self.setRoleInCoP(self.cop, 'user_sem_email', 'Participante')

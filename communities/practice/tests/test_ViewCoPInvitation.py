# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.generics.generics import encodeUTF8


class ViewCoPInvitationTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPInvitationTestCase, self).setUp()
        self.factoryCoP()
        self.view = self.getView(self.cop, 'viewCoPInvitation')

    def test_get_cop_context(self):
        self.assertEqual(
            self.view.get_cop_context(),
            self.cop
        )

    def test_get_communities_participating(self):
        self.assertEqual(self.view.get_communities_participating(), [])
        self.createTestContent(self.copcommunityfolder, 'CoP', 'cop', qtd=2)
        self.assertEqual(len(self.view.get_communities_participating()), 2)

    def test_send_invitation_request(self):
        self.assertFalse(self.view.send_invitation_request())
        self.view.request.form = {
            'submit': 'submit',
            'message': 'Mensagem',
            'email_list': 'teste@teste.com, a@teste.com',
            encodeUTF8(self.cop.Title()): self.cop.absolute_url(),
        }
        self.assertEqual(
            self.view.send_invitation_request(),
            self.cop.absolute_url()
        )
        self.createTestContent(self.copcommunityfolder, 'CoP', 'cop')
        new_cop = self.copcommunityfolder.cop_1
        self.view = self.getView(new_cop, 'viewCoPInvitation')
        self.view.request.form = {
            'submit': 'submit',
            'message': 'Mensagem',
            'email_list': 'teste@teste.com, a@teste.com',
            encodeUTF8(self.cop.Title()): self.cop.absolute_url(),
            encodeUTF8(new_cop.Title()): new_cop.absolute_url(),
        }
        self.assertEqual(
            self.view.send_invitation_request(),
            new_cop.absolute_url()
        )

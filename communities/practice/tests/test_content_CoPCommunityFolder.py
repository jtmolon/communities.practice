# -*- coding: utf-8 -*-

from communities.practice.config import OPCOES
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.subscribers import createMastersGroups
from communities.practice.subscribers import deleteMastersGroups
from communities.practice.generics.vocabularies import AVAILABLE_COLORS
from Products.CMFPlone.utils import getToolByName


class CoPCommunityFolderTestCase(IntegrationTestCase):

    def test_CoPCommunityFolder_addable(self):
        self.portal.invokeFactory('CoPCommunityFolder', 'copcommunityfolder',
                                   title="CoPCommunityFolder Title",
                                   description="CoPCommunityFolder Description",
                                   criacao_comunidades=OPCOES[0],)

        copcommunityfolder = self.portal.copcommunityfolder
        self.assertEquals(copcommunityfolder.Title(), "CoPCommunityFolder Title")
        self.assertEquals(copcommunityfolder.Description(), "CoPCommunityFolder Description")
        self.assertEquals(copcommunityfolder.getCriacao_comunidades(), OPCOES[0])
        self.assertTrue(copcommunityfolder.get_criacao_habilitado())
        copcommunityfolder.setCriacao_comunidades(OPCOES[1])
        self.assertFalse(copcommunityfolder.get_criacao_habilitado())
        self.assertEqual(copcommunityfolder.getColor(), AVAILABLE_COLORS[0])

    def test_CoPCommunityFolder_setLocalRoles(self):
        self.portal.invokeFactory('CoPCommunityFolder', 'copcommunityfolder',
                                   title="CoPCommunityFolder Title",
                                   description="CoPCommunityFolder Description",
                                   criacao_comunidades=OPCOES[0],)
        copcommunityfolder = self.portal.copcommunityfolder
        copcommunityfolder.at_post_create_script()
        self.assertEqual(copcommunityfolder.get_local_roles_for_userid('AuthenticatedUsers'), ('Contributor',))
        copcommunityfolder.at_post_edit_script()
        self.assertEqual(copcommunityfolder.get_local_roles_for_userid('AuthenticatedUsers'), ('Contributor',))
        copcommunityfolder.setCriacao_comunidades(OPCOES[1])
        copcommunityfolder.at_post_edit_script()
        self.assertFalse(copcommunityfolder.get_local_roles_for_userid('AuthenticatedUsers'))

    def test_CoPCommunityFolder_groups(self):
        self.factoryCoP()
        createMastersGroups(self.copcommunityfolder, False)
        portal_groups = getToolByName(self.portal, 'portal_groups')
        mm_id = "MM%s" % self.copcommunityfolder.UID()
        om_id = "OM%s" % self.copcommunityfolder.UID()
        self.assertIn(mm_id, portal_groups.getGroupIds())
        self.assertIn(om_id, portal_groups.getGroupIds())
        self.assertIn("Moderador_Master", self.copcommunityfolder.get_local_roles_for_userid(mm_id))
        self.assertIn("Observador_Master", self.copcommunityfolder.get_local_roles_for_userid(om_id))
        self.assertFalse(deleteMastersGroups(self.cop, False))
        self.assertTrue(deleteMastersGroups(self.copcommunityfolder, False))
        self.assertNotIn(mm_id, portal_groups.getGroupIds())
        self.assertNotIn(om_id, portal_groups.getGroupIds())

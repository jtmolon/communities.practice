# -*- coding: utf-8 -*-
import time
from plone.app.testing.selenium_layers import login
from communities.practice.testing import SeleniumTestCase

class FunctionalLogin(SeleniumTestCase):

    def setUp(self):
        self.driver = self.layer['selenium']
        self.portal = self.layer['portal']

    def loginPortal(self):
        """Login to the portal"""
        login(self.driver, self.portal)
        time.sleep(6)
        self.driver.find_element_by_id("personaltools-logout")

    def test_add_table(self):
        """Test Login portal"""
        self.loginPortal()

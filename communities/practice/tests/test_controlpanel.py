# -*- coding: utf-8 -*-
from plone.registry.interfaces import IRegistry
from Products.CMFCore.utils import getToolByName
from zope.component import getMultiAdapter
from zope.component import queryUtility

from communities.practice.interfaces import ICoPSettings
from communities.practice.tests.CoPBase import IntegrationTestCase


class ControlPanelTestCase(IntegrationTestCase):

    def test_registry_registered(self):
        registry = queryUtility(IRegistry)
        self.assertTrue(registry.forInterface(ICoPSettings))

    def test_coppla_controlpanel_view(self):
        view = getMultiAdapter((self.portal, self.portal.REQUEST),
                               name="coppla-settings")
        view = view.__of__(self.portal)
        self.assertTrue(view())

    def test_coppla_in_controlpanel(self):
        # Check if coppla is in the control panel
        self.controlpanel = getToolByName(self.portal, "portal_controlpanel")
        self.assertTrue('coppla' in [a.getAction(self)['id']
                            for a in self.controlpanel.listActions()])

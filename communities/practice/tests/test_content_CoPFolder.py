# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase

class CoPFolderTestCase(IntegrationTestCase):

    def setUp(self):
        super(CoPFolderTestCase, self).setUp()
        self.factoryCoPMenu()

    def test_CoPFolder_addable(self):
        self.copmenu.invokeFactory('CoPFolder', 'copfolder',
                                   title="CoPFolder Title",
                                   description="CoPFolder Description",)

        copfolder = self.copmenu.copfolder
        
        self.assertEquals(copfolder.Title(), "CoPFolder Title")
        self.assertEquals(copfolder.Description(), "CoPFolder Description")

    def test_CoPFolderChild_addable(self):
        self.factoryCoPFolder()
        self.copfolder.invokeFactory('CoPFolder', 'copfolderchild',
                                     title="CoPFolder Child Title",
                                     description="CoPFolder Child Description",)

        copfolderchild = self.copfolder.copfolderchild

        self.assertEquals(copfolderchild.Title(), "CoPFolder Child Title")
        self.assertEquals(copfolderchild.Description(), "CoPFolder Child Description")

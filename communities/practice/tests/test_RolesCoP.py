# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase

from plone.app.testing import TEST_USER_ID
from plone.app.testing import setRoles

class RolesCoP(IntegrationTestCase):

    def test_Aguardando_is_registered(self):
        self.assertTrue('Aguardando' in self.portal.valid_roles())

    def test_Bloqueado_is_registered(self):
        self.assertTrue('Bloqueado' in self.portal.valid_roles())

    def test_Participante_is_registered(self):
        self.assertTrue('Participante' in self.portal.valid_roles())

    def test_Moderador_is_registered(self):
        self.assertTrue('Moderador' in self.portal.valid_roles())

    def test_Moderador_Master_is_registered(self):
        self.assertTrue('Moderador_Master' in self.portal.valid_roles())

# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.generics.generics import allowTypes


class ViewCoPMenuTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPMenuTestCase, self).setUp()
        self.factoryCoPMenu()
        self.view = self.getView(self.copmenu, 'viewCoPMenu')

    def test_get_addable_types(self):
        allowTypes(self.copmenu, ['CoPATA', 'CoPEvent'])
        types = self.view.get_addable_types()
        self.assertEqual(types[0]['id'], 'CoPATA')
        self.assertEqual(types[1]['id'], 'CoPEvent')
        self.copmenu.setTitlemenu('calendario')
        types = self.view.get_addable_types()
        self.assertEqual(types[0]['id'], 'CoPEvent')
        self.assertEqual(types[1]['id'], 'CoPATA')

    def test_post_redirection(self):
        self.assertFalse(self.view.post_redirection())
        self.copmenu.setTitlemenu('posts')
        self.assertEqual(
            self.view.post_redirection(), self.cop.absolute_url()
        )

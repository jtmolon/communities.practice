# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.generics.generics import getProductInstalled
from communities.practice.tests.CoPUtils import installPseudoProduct
from communities.practice.config import INPUTS_FORM_IDS


class ViewCoPCustomEditTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPCustomEditTestCase, self).setUp()
        self.factoryCoP()
        self.view = self.getView(self.cop, 'viewCoPCustomEdit')

    def test_set_context_title(self):
        context_title = self.view.set_context_title()
        self.assertEqual(context_title['title'], 'Edição da Comunidade')

    def test_cop_forms_installed(self):
        self.assertEqual(
            self.view.cop_forms_installed(),
            getProductInstalled('cop.forms')
        )
        installPseudoProduct('cop.forms', 'installed')
        self.assertEqual(
            self.view.cop_forms_installed(),
            getProductInstalled('cop.forms')
        )

    def test_set_tools(self):
        self.view.set_tools()
        tools = self.view.set_tools()
        for tool in tools:
            self.assertEqual(
                getattr(self.cop, tool['id']) == 'Habilitar',
                tool['checked']
            )

    def test_update_data(self):
        self.assertFalse(self.view.update_data())
        self.request.form = {
            'submit_coppla_edit': True,
            'title': u'New CoP Title',
            'description': u'New description',
            'participar_input': u'Desabilitar',
        }
        for tool in INPUTS_FORM_IDS[:3]:
            self.request.form[tool] = u'Habilitar'
        self.assertTrue(self.view.update_data())
        self.assertEqual(self.cop.Title(), u'New CoP Title')
        self.assertEqual(self.cop.Description(), u'New description')
        self.assertEqual(self.cop.getParticipar_input(), u'Desabilitar')
        for tool in INPUTS_FORM_IDS[:3]:
            self.assertEqual(getattr(self.cop, tool), u'Habilitar')
        for tool in INPUTS_FORM_IDS[3:6]:
            self.assertEqual(getattr(self.cop, tool), u'Desabilitar')

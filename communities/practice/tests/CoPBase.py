# -*- coding: utf-8 -*-
from plone.app.testing import setRoles
from plone.app.testing import login
from plone.app.testing import TEST_USER_ID
from plone.app.testing import TEST_USER_NAME
from plone.app.testing import logout
from communities.practice.config import OPCOES 
from communities.practice.generics.cache import updateCommunityCache
import unittest2 as unittest
from communities.practice.testing import INTEGRATION_TESTING
from StringIO import StringIO
from zptlogo import zptlogo
from zope.security.management import queryInteraction
from zope.security.management import newInteraction, endInteraction
from communities.practice.subscribers import createMastersGroups
from zope.component import queryUtility
from plone.registry.interfaces import IRegistry
from plone.app.discussion.interfaces import IDiscussionSettings
from zope.component import createObject
from plone.app.discussion.interfaces import IConversation
from zope.component import queryMultiAdapter
from communities.practice.generics.generics import setMembersCoPRole
from communities.practice.subscribers import initialCreatedContents
from communities.practice.subscribers import createCoPGroup
from Products.CMFPlone.utils import getToolByName

class IntegrationTestCase(unittest.TestCase):
    
    layer = INTEGRATION_TESTING
    copcommunityfolder = None
    cop = None
    copmenu = None
    subcopmenu = None
    copfolder = None
    copfolderportfolio = None
    copportfolio = None
    coptarefas= None
    copupload = None
    coplink = None
    copimage = None
    copevent = None
    copdocument = None
    copfile = None
    copata = None
    copshare = None
    coppost = None
    subcop = None

    def setUp(self):
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        self.workflow_tool = self.portal.portal_workflow
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        login(self.portal, TEST_USER_NAME)
        if not queryInteraction():
            newInteraction()

    def tearDown(self):
        if queryInteraction():
            endInteraction()

    def factoryCoPCommunityFolder(self):
        if not self.copcommunityfolder:
            self.portal.invokeFactory('CoPCommunityFolder', 'copcommunityfolder',
                                      title="CoPCommunityFolder Title",
                                      description="CoPCommunityFolder Description",)
            self.copcommunityfolder = self.portal.copcommunityfolder
            createMastersGroups(self.copcommunityfolder, False)
        
    def factoryCoP(self, create_group=True):
        if not self.copcommunityfolder:
            self.factoryCoPCommunityFolder()
        if not self.cop:
            self.copcommunityfolder.invokeFactory('CoP', 'cop',
                                                  title="CoP Title",
                                                  description="CoP Description",
                                                  subject=("CoPSubject",),
                                                  acervo_input=OPCOES[0],
                                                  calendario_input=OPCOES[0],
                                                  forum_input=OPCOES[0],
                                                  portfolio_input=OPCOES[0],
                                                  tarefas_input=OPCOES[1],
                                                  exibir_todo_conteudo_input=OPCOES[0],)
            self.cop = self.copcommunityfolder.cop
            if create_group:
                createCoPGroup(self.cop)
            self.setRoleInCoP(self.cop, TEST_USER_ID, 'Moderador')
            updateCommunityCache(self.cop, TEST_USER_ID, 'Owner')
            updateCommunityCache(self.cop, TEST_USER_ID, 'Moderador')

    def factoryCoPMenu(self):
        if not self.cop:    
            self.factoryCoP()
        if not self.copmenu:
            self.cop.invokeFactory('CoPMenu', 'copmenu',
                                   title="CoPMenu Title",
                                   description="CoPMenu Description",)
            self.copmenu = self.cop.copmenu

    def factoryCoPFolder(self):
        if not self.copmenu:    
            self.factoryCoPMenu()
        if not self.copfolder:
            self.copmenu.invokeFactory('CoPFolder', 'copfolder',
                                       title="CoPFolder Title",
                                       description="CoPFolder Description",)
            self.copfolder = self.copmenu.copfolder

    def factoryCoPFolderPortfolio(self):
        if not self.cop:
            self.factoryCoP()

        if not self.copfolderportfolio:
            self.cop.invokeFactory('CoPMenu', 'copfolderportfolio',
                                   title="CoPFolderPortfolio Title",
                                   description="CoPFolderPortfolio Description",)
            self.copfolderportfolio = self.cop.copfolderportfolio

    def factoryCoPPortfolio(self):
        if not self.copfolderportfolio:
            self.factoryCoPFolderPortfolio()

        if not self.copportfolio:
            self.copfolderportfolio.invokeFactory('CoPPortfolio', 'copportfolio',
                                                  title="CoPPortfolio Title",
                                                 description="CoPPortfolio Description",)

            self.copportfolio = self.copfolderportfolio.copportfolio
    
    def factoryCoPTarefas(self):
        if not self.cop:
            self.factoryCoP()
        
        if not self.coptarefas:
            self.cop.invokeFactory('CoPMenu', 'coptarefas',
                                   title="CoPTarefas Title",
                                   description="CoPTarefas Description",)

            self.coptarefas = self.cop.coptarefas
     
    def factoryCoPUpload(self):
        if not self.coptarefas:
            self.factoryCoPTarefas()

        if not self.copupload:
            self.coptarefas.invokeFactory('CoPUpload', 'copupload',
                                           title="CoPUpload Title",
                                           description="CoPUpload Description",)

            self.copupload = self.coptarefas.copupload

    def factoryCoPLink(self):
        if not self.copfolder:
            self.factoryCoPFolder()
        if not self.coplink:
            self.copfolder.invokeFactory('CoPLink', 'coplink',
                                        title="CoPLink Title",
                                        description="CoPLink Description",
                                        remoteUrl="http://www.otics.org/otics",)
            self.coplink = self.copfolder.coplink

    def factoryCoPImage(self):
        if not self.copfolder:
            self.factoryCoPFolder()
        if not self.copimage:
            stringio = StringIO(zptlogo)
            self.copfolder.invokeFactory('CoPImage', 'copimage',
                                        title="CoPImage Title",
                                        description="CoPImage Description",
                                        image=stringio,)

            self.copimage = self.copfolder.copimage

    def factoryCoPEvent(self):
        if not self.copmenu:
            self.factoryCoPMenu()
        if not self.copevent:
            self.copmenu.invokeFactory('CoPEvent', 'copevent',
                                        title="CoPEvent Title",
                                        description="CoPEvent Description",
                                        location="CoPEvent Location",
                                        event_url="http://www.otics.org/otics",
                                        contact_name="CoPEvent Contact",
                                        contact_email="contact@mail.com",)
            self.copevent = self.copmenu.copevent

    def factoryCoPFile(self):
        if not self.copfolder:
            self.factoryCoPFolder()
        if not self.copfile:
            stringio = StringIO(zptlogo)
            self.copfolder.invokeFactory('CoPFile', 'copfile',
                                        title="CoPFile Title",
                                        description="CoPFile Description",
                                        file=stringio,)
            self.copfile = self.copfolder.copfile

    def factoryCoPDocument(self):
        if not self.copfolder:
            self.factoryCoPFolder()
        if not self.copdocument:
            self.copfolder.invokeFactory('CoPDocument', 'copdocument',
                                        title="CoPDocument Title",
                                        description="CoPDocument Description",
                                        )
            self.copdocument = self.copfolder.copdocument

    def factoryCoPATA(self):
        if not self.copmenu:
            self.factoryCoPMenu()
        if not self.copata:
            self.copmenu.invokeFactory('CoPATA', 'copata',
                                       title="CoPATA Title",
                                       description="CoPATA Description",
                                       pauta="<b>CoPATA</b> Pauta",
                                       discussao="<b>CoPATA</b> Discussao",
                                       encaminhamentos="<b>CoPATA</b> Encaminhamentos",)
            self.copata = self.copmenu.copata

    def factoryCoPShare(self):
        if not self.copmenu:
            self.factoryCoPMenu()
        if not self.copdocument:
            self.factoryCoPDocument()
        if not self.copshare:
            self.copmenu.invokeFactory('CoPShare', 'copshare',
                                        title="CoPShare Title",
                                        description="CoPShare Description",
                                        remoteUrl="http://www.otics.org/otics",)
            self.copshare = self.copmenu.copshare
            self.copshare.setParent_uid(self.copdocument.UID())

    def factoryCoPPost(self):
        if not self.copmenu:
            self.factoryCoPMenu()
        if not self.coppost:
            self.copmenu.invokeFactory(
                'CoPPost',
                'coppost',
                title="Post",
                description="CoPPost Description",
                )
            self.coppost = self.copmenu.coppost

    def factorySubCoPMenu(self):
        if not self.cop:
            self.factoryCoP()
        if not self.subcopmenu:
            self.cop.invokeFactory('CoPMenu', 'subcop',
                                    title="SubCoPMenu Title",
                                    description="SubCoPMenu Description",)
            self.subcopmenu = self.cop.subcop

    def factorySubCoP(self):
        if not self.cop:
            self.factoryCoP()
        if not self.subcopmenu:
            self.factorySubCoPMenu()
        if not self.subcop:
            self.subcopmenu.invokeFactory('CoP','subcop',
                                        title="SubCoP Title",
                                        description="SubCoP Description",
                                        subject=("SubCoPSubject",),
                                        acervo_input=OPCOES[0],
                                        calendario_input=OPCOES[0],
                                        forum_input=OPCOES[0],
                                        portfolio_input=OPCOES[0],
                                        tarefas_input=OPCOES[1],
                                        exibir_todo_conteudo_input=OPCOES[0],
                                        )
            self.subcop = self.subcopmenu.subcop

    def getView(self, context, view_name):
        view = queryMultiAdapter(
            (context, self.request),
            name=view_name
        )
        return view

    def changeLoggedMember(self, login_with=""):
        logout()
        if login_with:
            login(self.portal, login_with)

    def createComments(self, context, qtd=1):
        registry = queryUtility(IRegistry)
        settings = registry.forInterface(IDiscussionSettings)
        settings.globally_enabled = True
        conversation = IConversation(context)
        for i in xrange(qtd):
            comment = createObject('plone.Comment')
            comment.creator = TEST_USER_ID
            conversation.addComment(comment)        

    def create_basic_content(self):
        self.factoryCoPDocument()
        self.factoryCoPLink()
        self.factoryCoPImage()

    def createTestContent(self, context, portal_type, id_prefix, qtd=1):
        for i in xrange(1, qtd + 1):
            _id = '%s_%s' % (id_prefix, i)
            if not _id in context.objectIds():
                context.invokeFactory(
                    portal_type,
                    _id,
                    title=_id,
                    description=_id,
                )

    def createUsers(self, users_ids):
        for user_id in users_ids:
            self.portal.acl_users.userFolderAddUser(
                user_id, user_id, [], []
            )

    def setRoleInCoP(self, cop, user_id, role, recursive=True):
        setMembersCoPRole(cop, [user_id], role, recursive)

    def addUsersCoPActivity(self, cop):
        """Adds users for activity tests
        """
        users_ids = [
            'user_moderador', 'user_participante1',
            'user_participante2', 'user_participante3',
        ]
        self.createUsers(users_ids)
        self.setRoleInCoP(cop, users_ids[0], 'Moderador')
        for user_id in users_ids[1:]:
            self.setRoleInCoP(cop, user_id, 'Participante')

    def createContentActivity(self, cop, creator=''):
        """Creates content for activity tests
        """
        initialCreatedContents(cop, False)
        self.createTestContent(cop.acervo, 'CoPDocument', 'copdocument')
        registry = queryUtility(IRegistry)
        settings = registry.forInterface(IDiscussionSettings)
        settings.globally_enabled = True
        conversation = IConversation(cop.acervo.copdocument_1)
        comment = createObject('plone.Comment')
        if creator:
            comment.creator = creator
        conversation.addComment(comment)
        self.createTestContent(cop.portfolio, 'CoPPortfolio', 'copportfolio')
        copportfolio_1 = cop.portfolio.copportfolio_1
        self.createTestContent(copportfolio_1, 'CoPDocument', 'copdocument')
        self.createTestContent(cop.tarefas, 'CoPUpload', 'copupload')
        copupload_1 = cop.tarefas.copupload_1
        self.createTestContent(copupload_1, 'CoPFile', 'copfile')
        if creator:
            content = [
                cop.acervo.copdocument_1,
                copportfolio_1.copdocument_1,
                copupload_1.copfile_1,
            ]
            for obj in content:
                obj.setCreators([creator])
                obj.reindexObject()

    def setMembersData(self, members_ids):
        """Sets basic member data for a list of members by id
        """
        portal_membership = getToolByName(self.portal, 'portal_membership')
        for member_id in members_ids:
            member = portal_membership.getMemberById(member_id)
            member.setMemberProperties(
                mapping={"email": "test@test.com.br", "fullname": member_id}
            )

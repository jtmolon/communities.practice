# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.generics.generics import getMemberData
from communities.practice.subscribers import setTitleCoPPortfolio 

class CoPPortfolioTestCase(IntegrationTestCase):

    def setUp(self):
        super(CoPPortfolioTestCase, self).setUp()
        self.factoryCoPFolderPortfolio()

    def test_CoPPortfolio_addable(self):
        self.copfolderportfolio.invokeFactory('CoPPortfolio', 'copportfolio',
                                              title="CoPPortfolio Title",
                                              description="CoPPortfolio Description",)

        copportfolio = self.copfolderportfolio.copportfolio
        self.assertNotEqual(copportfolio.Title(), "CoPPortfolio Title")
        member_data = getMemberData(copportfolio, self.portal.portal_membership.getAuthenticatedMember().getId())
        setTitleCoPPortfolio(copportfolio, False)
        self.assertEquals(copportfolio.Title(), member_data[0])
        self.assertEquals(copportfolio.Description(), "CoPPortfolio Description")

    def test_CoPPortfolio_methods(self):
        self.factoryCoPPortfolio()
        copportfolio = self.copportfolio
        copportfolio.setTitle('CoPPortfolio Title')
        member = self.portal.portal_membership.getAuthenticatedMember()
        member.setMemberProperties(mapping={"fullname":"test"})
        setTitleCoPPortfolio(copportfolio, False)
        member_fullname = getMemberData(copportfolio, self.portal.portal_membership.getAuthenticatedMember().getId())[0]
        self.assertEquals(copportfolio.Title(), member_fullname)

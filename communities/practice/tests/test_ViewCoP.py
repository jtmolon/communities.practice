# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase


class ViewCoPTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPTestCase, self).setUp()
        self.factoryCoP()
        self.view = self.getView(self.cop, 'viewCoP')

    def test_get_filter_terms(self):
        self.assertEqual(len(self.view.get_filter_terms()), 5)

    def test_get_participation_community(self):
        self.setRoleInCoP(self.cop, 'test_user_1_', 'Moderador')
        participation = self.view.get_participation_community()
        self.assertEqual(participation['participation'], 'Criador')
        self.assertEqual(participation['review_state'], 'Restrito')
        self.assertTrue(participation['accepts_shares'])
        self.workflow_tool.doActionFor(self.cop, 'publico')
        self.cop.setParticipar_input('Desabilitar')
        self.cop.reindexObject()
        participation = self.view.get_participation_community()
        self.assertEqual(participation['review_state'], 'Publico')
        self.assertFalse(participation['accepts_shares'])
        self.createUsers(['member'])
        self.changeLoggedMember('member')
        participation = self.view.get_participation_community()
        self.assertEqual(participation['participation'], 'Autenticado')
        self.setRoleInCoP(self.cop, 'member', 'Participante')
        participation = self.view.get_participation_community()
        self.assertEqual(participation['participation'], 'Participante')

    def test_participation_allowed(self):
        self.assertTrue(self.view.participation_allowed())
        self.cop.setParticipar_input('Desabilitar')
        self.assertFalse(self.view.participation_allowed())
        self.factorySubCoP()
        self.view = self.getView(self.subcop, 'viewCoP')
        self.createUsers(['member'])
        self.changeLoggedMember('member')
        self.assertFalse(self.view.participation_allowed())
        self.setRoleInCoP(self.cop, 'member', 'Participante')
        self.assertTrue(self.view.participation_allowed())

    def test_create_post(self):
        self.factoryCoPMenu()
        self.view.context.posts = self.copmenu
        self.assertFalse(self.view.create_post())
        self.view.request.form = {'post_message': 'post_message'}
        self.assertTrue(self.view.create_post())

    def test_can_add_cop_post(self):
        self.assertTrue(self.view.can_add_cop_post())
        self.changeLoggedMember()
        self.assertFalse(self.view.can_add_cop_post())

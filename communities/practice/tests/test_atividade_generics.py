# -*- coding: utf-8 -*-
from datetime import datetime
from datetime import timedelta

from DateTime import DateTime

from Products.CMFPlone.utils import getToolByName

from communities.practice.config import OPCOES
from communities.practice.generics import vocabularies
from communities.practice.generics.atividade_generics import \
    exportCoPCommunityFolderAtividade, \
    getCoPAtividadeCommunities, \
    getCoPAtividadeContent, \
    getCoPAtividadeLenMembers, \
    getCoPAtividadeMembers, \
    getCoPAtividadeTotalTypes, \
    getCoPAtividadeTypes, \
    getCoPCommunityFolderAtividadeTypes, \
    getStartEndDate, \
    TYPES_ATIVIDADE
from communities.practice.subscribers import initialCreatedContents
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.tests.CoPUtils import \
    ALL_MEMBERS, FILTERED_MEMBERS


class AtividadeGenericsTestCase(IntegrationTestCase):

    def setUp(self):
        super(AtividadeGenericsTestCase, self).setUp()
        self.factorySubCoP()
        self.cop.setTarefas_input(OPCOES[0])
        initialCreatedContents(self.cop, False)
        self.addUsersCoPActivity(self.cop)
        self.createContentActivity(self.cop, 'user_participante1')

    def test_atividade_generics_getCoPAtividadeMembers(self):
        member_list = getCoPAtividadeMembers(self.cop)
        self.assertItemsEqual(member_list, ALL_MEMBERS)

        input_filter = "user_participante"
        member_list = getCoPAtividadeMembers(self.cop, input_filter)
        self.assertItemsEqual(member_list, FILTERED_MEMBERS)

    def test_atividade_generics_getCoPAtividadeContent(self):
        user_participante1 = ('user_participante1', 'user_participante1')

        member_list = getCoPAtividadeMembers(self.cop)

        folder = "comunidade"
        content = getCoPAtividadeContent(self.cop, member_list, folder)
        self.assertItemsEqual(content.keys(), ALL_MEMBERS)
        self.assertEqual(
            content[user_participante1][('total', 'Total')], 4,
        )
        self.assertEqual(
            content[user_participante1][('CoPDocument', u'Páginas')], 2,
        )
        self.assertEqual(
            content[user_participante1][('CoPFile', u'Arquivos')], 1,
        )

        folder = "acervo"
        content = getCoPAtividadeContent(self.cop, member_list, folder)
        self.assertItemsEqual(content.keys(), ALL_MEMBERS)
        self.assertEqual(
            content[user_participante1][('total', 'Total')], 1,
        )
        self.assertEqual(
            content[user_participante1][('CoPDocument', u'Páginas')], 1,
        )
        self.assertEqual(
            content[user_participante1][('CoPFile', u'Arquivos')], 0,
        )

        folder = "portfolio"
        content = getCoPAtividadeContent(self.cop, member_list, folder)
        self.assertItemsEqual(content.keys(), ALL_MEMBERS)
        self.assertEqual(
            content[user_participante1][('total', 'Total')], 1,
        )
        self.assertEqual(
            content[user_participante1][('CoPDocument', u'Páginas')], 1,
        )
        self.assertEqual(
            content[user_participante1][('CoPFile', u'Arquivos')], 0,
        )

        folder = "tarefas"
        content = getCoPAtividadeContent(self.cop, member_list, folder)
        self.assertItemsEqual(content.keys(), ALL_MEMBERS)
        self.assertEqual(
            content[user_participante1][('total', 'Total')], 1,
        )
        self.assertEqual(
            content[user_participante1][('CoPFile', u'Arquivos')], 1,
        )

        old_date = self.cop.acervo.created()
        self.cop.acervo.copdocument_1.setCreationDate(
            DateTime(datetime.now() - timedelta(days=1)),
        )
        self.cop.acervo.copdocument_1.reindexObject()
        folder = "comunidade"
        today = datetime.today()
        today = DateTime("%s/%s/%s" % (today.year, today.month, today.day))
        start_period = today
        end_period = today + 1
        content = getCoPAtividadeContent(
            self.cop, member_list, folder, [start_period, end_period],
        )
        self.assertItemsEqual(content.keys(), ALL_MEMBERS)
        self.assertEqual(
            content[user_participante1][('total', 'Total')], 3,
        )
        self.assertEqual(
            content[user_participante1][('CoPDocument', u'Páginas')], 1,
        )
        self.assertEqual(
            content[user_participante1][('CoPFile', u'Arquivos')], 1,
        )
        self.cop.acervo.copdocument_1.setCreationDate(old_date)
        self.cop.acervo.copdocument_1.reindexObject()

    def test_atividade_generics_getCoPAtividadeTypes(self):
        folder = "comunidade"
        types = getCoPAtividadeTypes(folder, True, self.cop)
        self.assertItemsEqual(
            types, TYPES_ATIVIDADE.get(folder) + [('total', u'Total')]
        )

        folder = "acervo"
        types = getCoPAtividadeTypes(folder)
        self.assertItemsEqual(
            types, TYPES_ATIVIDADE.get(folder) + [('total', u'Total')]
        )

        folder = "portfolio"
        types = getCoPAtividadeTypes(folder)
        self.assertItemsEqual(
            types, TYPES_ATIVIDADE.get(folder) + [('total', u'Total')]
        )

        folder = "tarefas"
        types = getCoPAtividadeTypes(folder)
        self.assertItemsEqual(types, TYPES_ATIVIDADE.get(folder))

        self.cop.setAvailable_forms((
            'BFCheckList', 'BFGoodPracticeReport', 'BFVisitAcknowledge',
        ))
        folder = "formularios"
        types = getCoPAtividadeTypes(folder, True, self.cop)
        self.assertItemsEqual(types, [
            ('BFCheckList', u'Checklist'),
            ('BFGoodPracticeReport', u'Boas Práticas'),
            ('BFVisitAcknowledge', u'Relato de Visita'),
            ('total', u'Total'),
        ])
        self.cop.setAvailable_forms((
            'BFCheckList', 'BFGoodPracticeReport',
            'BFVisitAcknowledge', 'CoPCaseDescription',
        ))
        types = getCoPAtividadeTypes(folder, True, self.cop)
        self.assertItemsEqual(types, [
            ('BFCheckList', u'Checklist'),
            ('BFGoodPracticeReport', u'Boas Práticas'),
            ('BFVisitAcknowledge', u'Relato de Visita'),
            ('CoPCaseDescription', u'Descrição de Casos'),
            ('total', u'Total'),
        ])

        folder = "comunidade"
        types = getCoPAtividadeTypes(folder, True, self.cop)
        grouped_forms = (
            'BFCheckList', 'BFGoodPracticeReport',
            'BFVisitAcknowledge', 'CoPCaseDescription'
        )
        self.assertItemsEqual(
            types,
            TYPES_ATIVIDADE.get(folder) +
            [(grouped_forms, u'Formulários')] +
            [('total', u'Total')],
        )

    def test_atvidade_generics_getCoPCommunityFolderAtividadeTypes(self):
        types = getCoPCommunityFolderAtividadeTypes()
        extras = [('total', u'Total')]
        forms = []
        for form, description in vocabularies.AVAILABLE_FORMS_VOCABULARY:
            forms.append(form)
        if forms:
            extras.append((tuple(forms), u'Formulários'))
        self.assertItemsEqual(types, TYPES_ATIVIDADE.get("comunidade") + extras)

    def test_atividade_generics_getCoPAtividadeTotalTypes(self):
        folder = "copcommunityfolder"
        total_types = getCoPAtividadeTotalTypes(self.cop, folder)
        self.assertEqual(total_types[('total', 'Total')], 5)
        self.assertEqual(total_types[('CoPDocument', u'Páginas')], 3)
        self.assertEqual(total_types[('CoPFile', u'Arquivos')], 1)

        folder = "comunidade"
        total_types = getCoPAtividadeTotalTypes(self.cop, folder)
        self.assertEqual(total_types[('total', 'Total')], 5)
        self.assertEqual(total_types[('CoPDocument', u'Páginas')], 3)
        self.assertEqual(total_types[('CoPFile', u'Arquivos')], 1)

        folder = "acervo"
        total_types = getCoPAtividadeTotalTypes(self.cop, folder)
        self.assertEqual(total_types[('total', 'Total')], 2)
        self.assertEqual(total_types[('CoPDocument', u'Páginas')], 2)

        folder = "portfolio"
        total_types = getCoPAtividadeTotalTypes(self.cop, folder)
        self.assertEqual(total_types[('total', 'Total')], 1)
        self.assertEqual(total_types[('CoPDocument', u'Páginas')], 1)

        folder = "tarefas"
        total_types = getCoPAtividadeTotalTypes(self.cop, folder)
        self.assertEqual(total_types[('CoPFile', u'Arquivos')], 1)

        old_date = self.cop.acervo.created()
        self.cop.acervo.copdocument_1.setCreationDate(
            DateTime(datetime.now() - timedelta(days=1))
        )
        self.cop.acervo.copdocument_1.reindexObject()
        folder = "comunidade"
        today = datetime.today()
        today = DateTime("%s/%s/%s" % (today.year, today.month, today.day))
        start_period = today
        end_period = today + 1
        total_types = getCoPAtividadeTotalTypes(
            self.cop, folder, [start_period, end_period],
        )
        self.assertEqual(total_types[('total', 'Total')], 4)
        self.assertEqual(total_types[('CoPDocument', u'Páginas')], 2)
        self.assertEqual(total_types[('CoPFile', u'Arquivos')], 1)
        self.cop.acervo.copdocument_1.setCreationDate(old_date)
        self.cop.acervo.copdocument_1.reindexObject()

    def test_atividade_generics_getCoPAtividadeLenMembers(self):
        len_members = getCoPAtividadeLenMembers(self.cop.atividade)
        self.assertEqual(len_members, 5)

    def test_getCoPAtividadeCommunities(self):
        portal_catalog = getToolByName(self.portal, "portal_catalog")
        query_path = "/".join(self.copcommunityfolder.getPhysicalPath())
        catalog_communities = portal_catalog(path=query_path,
                                             portal_type="CoP")
        communities = getCoPAtividadeCommunities(self.copcommunityfolder)
        self.assertEqual(len(communities), len(catalog_communities))
        self.assertItemsEqual(
            [community['brain'].UID for community in communities],
            [community.UID for community in catalog_communities],
        )

    def test_exportCoPCommunityFolderAtividade(self):
        exportCoPCommunityFolderAtividade(
            self.request, self.copcommunityfolder,
        )
        filename = "Atividades_%s_%s.csv" % (
            self.copcommunityfolder.Title(),
            datetime.now().strftime("%d_%m_%Y"),
        )
        check_headers = {
            'content-length': '0',
            'Content-Type': 'text/csv',
            'content-disposition': 'attachment; filename="%s"' % filename,
        }
        self.assertEqual(self.request.response.headers, check_headers)
        self.assertEqual(self.request.response.status, 200)

    def test_getStartEndDate(self):
        form = {}
        start_date = DateTime("2000/01/01")
        end_date = DateTime(datetime.today() + timedelta(days=1))
        period = getStartEndDate(form)
        start_period = period[0]
        end_period = period[1]
        self.assertEqual(start_period, start_date)
        self.assertEqual(end_period.Date(), end_date.Date())
        form["submit_copactivity"] = "Filtrar"
        form["start_date_copactivity"] = "2014-06-01 00:00"
        form["end_date_copactivity"] = "2014-06-08 00:00"
        period = getStartEndDate(form)
        self.assertEqual(period[0], DateTime("2014-06-01 00:00"))
        self.assertEqual(period[1], DateTime("2014-06-08 00:00") + 1)

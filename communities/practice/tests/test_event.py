# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.event import CoPLocalRolesChangedEvent
from communities.practice.event import CoPMasterRolesChangedEvent

class EventTestCase(IntegrationTestCase):
    
    def test_CoPLocalRolesChanged(self):
        self.factoryCoP()
        event = CoPLocalRolesChangedEvent(
            self.cop,
            "member_id",
            "Participante",
        )
        self.assertEqual(event.object, self.cop)
        self.assertEqual(event.member_id, "member_id")
        self.assertEqual(event.role, "Participante")
    
    def test_CoPMasterRolesChanged(self):
        self.factoryCoPCommunityFolder()
        event = CoPMasterRolesChangedEvent(
            self.copcommunityfolder,
            "member_id",
            "Moderador_Master",
        )
        self.assertEqual(event.object, self.copcommunityfolder)
        self.assertEqual(event.member_id, "member_id")
        self.assertEqual(event.role, "Moderador_Master")

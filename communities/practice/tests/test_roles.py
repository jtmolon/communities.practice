# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.tests.CoPUtils import createStructure
from communities.practice.generics.generics import setMembersCoPRole
from communities.practice.generics.generics import setMasterRole
from communities.practice.generics.generics import getMMGroupMembers
from communities.practice.generics.generics import getOMGroupMembers
from Products.CMFPlone.utils import getToolByName
from xml.dom import minidom
import os

class CoPRolesTestCase(IntegrationTestCase):

    def test_participante_propagation(self):
        self.assert_xml_roles('roles/propagationFromParticipante.xml')
        self.assert_xml_roles('roles/propagationFromBloqueado.xml')
        self.assert_xml_roles('roles/propagationFromModerador.xml')
        self.assert_xml_roles('roles/propagationFromObservador.xml')
        self.assert_xml_roles('roles/propagationFromNothing.xml')
        self.assert_xml_roles('roles/propagationFromMaster.xml')

    def assert_xml_roles(self, roles_file):
        PROJECT_PATH = os.path.abspath(os.path.dirname(__file__))
        file_name = ("%s/%s") % (PROJECT_PATH, roles_file)
        try:
            doc = minidom.parse(file_name)
        except:
            return False

        portal = self.layer['portal']
        portal_catalog = getToolByName (portal, 'portal_catalog')
        transitions = doc.getElementById('transitions')
        for transition in transitions.getElementsByTagName('transition'):
            self.create_cop_structure("roles/initial_roles.xml")
            user_id = transition.getAttribute('user')
            new_role = transition.getAttribute('new_role')
            context_id = transition.getAttribute('context')
            brain = portal_catalog(id=context_id)
            if brain:
                context = brain[0].getObject()
                #propagate role
                if new_role in ['Moderador_Master', 'Observador_Master']:
                    setMasterRole(context, user_id, new_role)
                elif new_role == 'Excluir_Master':
                    setMasterRole(context, user_id, "Excluir")
                else:
                    setMembersCoPRole(context, [user_id], new_role)

                for test_context_node in transition.getElementsByTagName('context'):
                    test_context_id = test_context_node.getAttribute('id')
                    brain = portal_catalog(id=test_context_id)
                    if brain:
                        test_context = brain[0].getObject()
                        mm_members = getMMGroupMembers(test_context)
                        om_members = getOMGroupMembers(test_context)
                        local_roles = test_context.get_local_roles_for_userid(user_id)
                        roles = test_context_node.getElementsByTagName('role')
#                        print roles_file
#                        print "Context:%s\tUser:%s"%(test_context_id, user_id)
#                        print local_roles, [role.firstChild.nodeValue for role in roles]
                        for role in roles:
                            role_value = role.firstChild.nodeValue
                            if role_value == "Moderador_Master":
                                self.assertIn(user_id, mm_members)
                            elif role_value == "Observador_Master":
                                self.assertIn(user_id, om_members)
                            else:
                                self.assertIn(role_value, local_roles)
                        if not roles:
                            self.assertFalse(local_roles)

    def create_cop_structure(self, input_file_name):
        portal = self.layer['portal']
        createStructure(portal, input_file_name)

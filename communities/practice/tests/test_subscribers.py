# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase

from StringIO import StringIO
from zptlogo import zptlogo
from Products.CMFPlone.utils import getToolByName
from Products.Archetypes.event import ObjectInitializedEvent
from plone.app.testing import TEST_USER_ID
from communities.practice.generics.cache import cleanMemberCache
from communities.practice.generics.cache import getCoPLocalRoles
from communities.practice.generics.cache import listMemberRoleCommunitiesParticipating
from communities.practice.subscribers import initialCreatedContents
from communities.practice.subscribers import membersCommunitiesCache
from communities.practice.subscribers import cleanMembersCommunitiesCache
from communities.practice.subscribers import reindexSuperCoP
from communities.practice.subscribers import removeCoPFromCache


class SubscribersTestCase(IntegrationTestCase):

    def setUp(self):
        super(SubscribersTestCase, self).setUp()
        self.factoryCoP()
        self.pm = getToolByName(self.cop, 'portal_membership')
        self.createUsers(['user'])

    def test_reindexSuperCoP(self):
        stringio = StringIO(zptlogo)
        self.copcommunityfolder.setImagem(stringio)

        catalog = getToolByName(self.cop, 'portal_catalog')
        path = '/'.join(self.copcommunityfolder.getPhysicalPath())
        query = {}
        query['path'] = {'query': path, 'depth': 1}
        query['portal_type'] = "CoP"
        brain = catalog(query)
        cop = brain[0]
        self.assertEqual(cop.subCoPNumber, 0)
        self.factorySubCoP()
        initialized_event = ObjectInitializedEvent(self.subcop)
        reindexSuperCoP(self.subcop, initialized_event)
        brain = catalog(query)
        cop = brain[0]
        self.assertEqual(cop.subCoPNumber, 1)

    def test_removeCoPFromCache(self):
        cop_cache = getCoPLocalRoles(self.cop, brain=False)
        cop_cache = cop_cache.get(self.cop.UID())
        member_cache = listMemberRoleCommunitiesParticipating(TEST_USER_ID)
        member_cache = member_cache.get(TEST_USER_ID)
        self.assertTrue(cop_cache)
        self.assertIn(self.cop.UID(), member_cache.keys())
        self.cop.REQUEST.set('link_integrity_events_counter',2)
        self.assertTrue(removeCoPFromCache(self.cop, False))
        self.cop.REQUEST.set('link_integrity_events_counter',2)
        self.assertFalse(removeCoPFromCache(self.cop, False))
        self.assertFalse(cleanMemberCache(TEST_USER_ID))

    def test_membersCommunitiesCache(self):
        user = self.pm.getMemberById('user')
        communities = membersCommunitiesCache(user, False)
        self.assertFalse(communities['user'])
        self.setRoleInCoP(self.cop, 'user', 'Participante')
        communities = membersCommunitiesCache(user, False)
        self.assertTrue(communities['user'])

    def test_cleanMembersCommunitiesCache(self):
        user = self.pm.getMemberById('user')
        self.assertFalse(cleanMembersCommunitiesCache(user, False))
        self.setRoleInCoP(self.cop, 'user', 'Participante')
        membersCommunitiesCache(user, False)
        self.assertTrue(cleanMembersCommunitiesCache(user, False))

    def test_create_content(self):
        self.factorySubCoP()
        initialCreatedContents(self.subcop, False)
        self.assertTrue(self.subcop.__ac_local_roles_block__)

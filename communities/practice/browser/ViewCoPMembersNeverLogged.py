# -*- coding: utf-8 -*-
from DateTime import DateTime
from Products.CMFPlone.utils import getToolByName
from zope.component.hooks import getSite

from communities.practice.browser.ViewCoPCommunityFolder import \
    ViewCoPCommunityFolder
from communities.practice.generics.asyncmail import createMessage
from communities.practice.generics.asyncmail import sendAsyncMail
from communities.practice.generics.generics import encodeUTF8
from communities.practice.generics.generics import getCoPSettings
from communities.practice.generics.generics import getMemberData


class ViewCoPMembersNeverLogged(ViewCoPCommunityFolder):
    """Default view para relatorio de MembersNeverLogged.
    """

    def set_cop_menu_selected(self):
        self.cop_menu_selected = 'copcommunityneverlogged'
        return self.cop_menu_selected

    def set_context_title(self):
        self.context_title = {}
        self.context_title['title'] = "Usuários que nunca fizeram login"
        self.context_title['description'] = """
            Envie uma mensagem para os membros que se cadastraram
            mas nunca fizeram login no portal
            """
        return self.context_title

    def send_message_members(self):
        """Envia mensagem para os membros.
        """
        form = self.request.form
        if 'submit_send_message_members' in form.keys():
            portal_membership = getToolByName(
                self.context, 'portal_membership'
            )
            authenticated = portal_membership.getAuthenticatedMember()
            member_data = getMemberData(self.context, authenticated.getId())
            moderador_nome = encodeUTF8(member_data[0])
            portal = getSite()
            url = encodeUTF8(portal.absolute_url())

            assunto = encodeUTF8(form.get("assunto"))
            mensagem = ""
            if form.get("mensagem"):
                mensagem = 'Content-Type: text/plain;'
                mensagem += 'charset="UTF-8"\nMIME-Version: 1.0\n\n'
                mensagem += encodeUTF8(form.get("mensagem"))
                mensagem += '\n\n\nLink para acesso ao portal:\n%s' % (url)
                mensagem += '\n\nMensagem enviada por: %s' % (moderador_nome)
            if assunto and mensagem:
                mail_to = []
                for user_id, valor in form.items():
                    if valor == 'Sim':
                        mail = getMemberData(self.context, user_id)[1]
                        if mail:
                            mail_to.append(mail)
                if mail_to:
                    settings = getCoPSettings()
                    mail_from = settings.username
                    createMessage(
                        mail_from, mail_to, assunto,
                        mensagem, self.request.getURL())
                    sendAsyncMail()
                    status_message = u"Email enviado com sucesso."
                    message_type = 'info'
                else:
                    status_message = u"Selecione emails para envio."
                    message_type = 'error'
            else:
                status_message = u"Campos obrigatórios não preenchidos"
                message_type = 'error'
            self.context.plone_utils.addPortalMessage(
                status_message, message_type
            )
            return self.request.response.redirect(
                self.get_action_url()
            )
        return ""

    def get_members(self):
        """Retorna quem nao se logou ainda
        """
        membership_tool = getToolByName(self.context, 'portal_membership')
        members = membership_tool.listMembers()
        mail_to = []
        for member in members:
            never_logged = DateTime('2000/01/01 00:00:00 GMT-2')
            last_login = member.getProperty('last_login_time')
            if last_login.equalTo(never_logged):
                mail_to.append({
                    'id': member.getProperty('id', ''),
                    'nome': member.getProperty('fullname', ''),
                })
        if mail_to:
            mail_to.sort(key=lambda item: item['nome'])
        return mail_to

    def get_action_url(self):
        """ Returns form action URL
        """
        return "%s/viewCoPMembersNeverLogged" % self.context.absolute_url()

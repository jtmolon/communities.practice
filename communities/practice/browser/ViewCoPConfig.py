# -*- coding: utf-8 -*-
from communities.practice.browser.ViewCoPBase import ViewCoPBase
from communities.practice.generics.generics import getMemberData
from communities.practice.generics.generics import encodeUTF8
from communities.practice.generics.generics import setMembersCoPRole
from communities.practice.generics.asyncmail import createMessage
from communities.practice.generics.asyncmail import sendAsyncMail
from communities.practice.generics.generics import hasSuperRole
from communities.practice.generics.generics import getSuperCoP
from communities.practice.generics.generics import isSubCoP
from communities.practice.generics.generics import getCoPParticipants
from communities.practice.generics.generics import getCoPSettings
from communities.practice.generics.generics import getReviewStateDescription
from communities.practice.generics.generics import getReviewStateTitle
from communities.practice.generics.generics import searchPortalUsers
from communities.practice.generics.generics import getCoPContext
from zope.component.hooks import getSite
from Acquisition import aq_parent
from Products.CMFPlone.utils import getToolByName
from zope.security import checkPermission
import re


class ViewCoPConfig(ViewCoPBase):
    """Default View for CoPConfig"""

    def get_users(self):
        user_config = self.request.get("user_config", None)
        if user_config == "add":
            self.users = self.get_add_participants()
        elif user_config == "pending":
            self.users = self.get_pending_participants()
        else:
            self.users = self.get_participants()
        return self.users

    def set_users(self):
        user_config = self.request.get("user_config", None)
        if user_config == "add":
            self.users = self.set_add_participants()
        elif user_config == "pending":
            self.users = self.set_pending_participants()
        else:
            self.users = self.set_participants()
        return self.users

    def get_participants(self):
        portal = getSite()
        community = getCoPContext(self.context)
        portal_url = portal.absolute_url()
        local_roles = community.get_local_roles()
        users = []
        transitions = ['Participante', 'Moderador', 'Observador',
                       'Bloqueado', 'Excluir', ]
        for user_id, roles in local_roles:
            if 'Aguardando' not in roles:
                member_data = getMemberData(community, user_id)
                if member_data:
                    user_name = member_data[0]
                    user_role = self.get_cop_role(user_id)
                    super_role = hasSuperRole(community, user_id)
                    super_user = super_role or user_role == "Owner"
                    users.append({
                        "full_name": user_name.capitalize(),
                        "id": user_id,
                        "role": user_role,
                        "transitions": transitions if not super_user else [],
                        "author_url": "%s/author/%s" % (portal_url, user_id),
                        "super": super_user,
                    })
        users.sort(key=lambda user: user['full_name'])
        return users

    def set_participants(self):
        if self.request.get('update_roles'):
            community = getCoPContext(self.context)
            cop_participants = [user['id'] for user in self.get_participants()]
            portal_groups = getToolByName(self, 'portal_groups')
            group_id = community.UID()
            is_sub_cop = isSubCoP(community)

            form_items = self.request.form.items()
            modified_roles = {}
            for user, roles in form_items:
                new_role = roles[0] if isinstance(roles, list) else None
                old_role = roles[1] if isinstance(roles, list) else None
                if new_role != old_role and \
                   user in cop_participants and \
                   not hasSuperRole(community, user):
                    if new_role not in modified_roles:
                        modified_roles[new_role] = []
                    modified_roles[new_role].append(user)

            for role, users in modified_roles.items():
                if role == 'Excluir' and not is_sub_cop and \
                   group_id in portal_groups.getGroupIds():
                    for user in users:
                        portal_groups.removePrincipalFromGroup(user, group_id)
                setMembersCoPRole(community, users, role)
            self.context.plone_utils.addPortalMessage(
                u'As alterações foram salvas', 'info'
            )
            return self.request.response.redirect(
                self.context.absolute_url()
            )
        return None

    def get_add_participants(self):
        search_filter = self.request.get('SearchableText', [])
        users = []
        if len(search_filter) < 1:
            return users
        community = getCoPContext(self.context)
        portal = getSite()
        portal_url = portal.absolute_url()
        local_roles = community.get_local_roles()
        users_comunidade = []
        for user_id, roles in local_roles:
            users_comunidade.append(user_id)
        transitions = ["Selecionar", "Participante", "Observador"]

        if isSubCoP(community):
            superCoP = getSuperCoP(community)
            parent_users = superCoP.get_local_roles()
            for user_id, roles in parent_users:
                ignored_roles = ["Bloqueado", "Aguardando", "Observador"]
                parent_roles = [role for role in roles
                                if role not in ignored_roles]
                if parent_roles and user_id not in users_comunidade:
                    member_data = getMemberData(community, user_id)
                    if member_data:
                        user_name = member_data[0]
                        filtered = \
                            re.search(search_filter, user_name, re.I) or \
                            re.search(search_filter, user_id, re.I)
                        if filtered:
                            author_url = "%s/author/%s" % (portal_url, user_id)
                            users.append({
                                "full_name": user_name.capitalize(),
                                "id": user_id,
                                "role": "Selecionar",
                                "transitions": transitions,
                                "author_url": author_url,
                                "super": False,
                            })
        else:
            users = searchPortalUsers(self.context, self.request,
                                      search_filter, users_comunidade)
            for user in users:
                user["role"] = "Selecionar"
                user["transitions"] = transitions
                user["super"] = False
        return users

    def set_add_participants(self):
        if self.request.get("update_roles"):
            community = getCoPContext(self.context)
            form_items = self.request.form.items()
            mail_to_participants = []
            mail_to_observers = []
            is_sub_cop = isSubCoP(community)
            portal_groups = getToolByName(self, 'portal_groups')
            group_id = community.UID()
#           verifica se usuario esta no local roles da supercop
            if is_sub_cop:
                super_cop = getSuperCoP(community)
                super_roles = getCoPParticipants(super_cop)

            modified_roles = {}
            for user_id, roles in form_items:
                new_role = roles[0] if isinstance(roles, list) else None
                if new_role in ["Participante", "Observador"]:
                    if not is_sub_cop or (is_sub_cop and user_id in super_roles):
                        if not is_sub_cop and group_id in portal_groups.getGroupIds():
                            portal_groups.addPrincipalToGroup(user_id, group_id)
#                       cria lista de usuarios para adicionar a comunidade
                        if new_role not in modified_roles:
                            modified_roles[new_role] = []
                        modified_roles[new_role].append(user_id)

#                       prepara lista de email de notificacao de insercao de participante
                        member_data = getMemberData(community, user_id)
                        if member_data:
                            email = member_data[0]
                            if new_role == "Participante":
                                mail_to_participants.append(email)
                            else:
                                mail_to_observers.append(email)

            for role, users in modified_roles.items():
                recursive = role == "Observador"
                setMembersCoPRole(community, users, role, recursive)

            mail_lists = (
                (mail_to_participants, "Participante"),
                (mail_to_observers, "Observador"),
            )
            settings = getCoPSettings()
            mail_from = settings.username
            subject = encodeUTF8("Participação em Comunidade")
            for mail_to, role in mail_lists:
                if mail_to:
                    message = encodeUTF8("Você foi adicionado como %s da comunidade %s.\n" % (role, community.Title()))
                    message += encodeUTF8("Para acessar a comunidade clique em %s" % (community.absolute_url()))
                    createMessage(mail_from, mail_to, subject, message, self.context.absolute_url())
                    sendAsyncMail()

            self.context.plone_utils.addPortalMessage(
                u'Os novos participantes foram adicionados', 'info'
            )
            return self.request.response.redirect(
                self.context.absolute_url()
            )

        return None

    def get_pending_participants(self):
        community = getCoPContext(self.context)
        portal = getSite()
        local_roles = community.users_with_local_role('Aguardando')
        users = []
        for user_id in local_roles:
            member_data = getMemberData(community, user_id)
            if member_data:
                user_name = member_data[0]
                author_url = "%s/author/%s" % (portal.absolute_url(), user_id)
                users.append({
                    "full_name": user_name.capitalize(),
                    "id": user_id,
                    "role": 'Aguardando',
                    "transitions": ['Aguardando', 'Aprovar', 'Negar'],
                    "author_url": author_url,
                    "super": False,
                })

        users.sort(key=lambda item: item['full_name'])
        return users

    def set_pending_participants(self):
        if self.request.get("update_roles"):
            community = getCoPContext(self.context)
            portal_membership = getToolByName(self.context, 'portal_membership')
            form_items = self.request.form.items()

            pending = [user['id'] for user in self.get_pending_participants()]

            approved_list = []
            denied_list = []
            portal_groups = getToolByName(self, 'portal_groups')
            group_id = community.UID()
            is_sub_cop = isSubCoP(community)
            modified_roles = {}
            for user, roles in form_items:
                new_role = roles[0] if isinstance(roles, list) else None
                old_role = roles[1] if isinstance(roles, list) else None
                if new_role != old_role and user in pending:
                    member_data = getMemberData(community, user)
                    if member_data:
                        email = member_data[1]
                        if new_role == 'Aprovar':
                            if not is_sub_cop and group_id in portal_groups.getGroupIds():
                                portal_groups.addPrincipalToGroup(user, group_id)
                            if 'Participante' not in modified_roles:
                                modified_roles['Participante'] = []
                            modified_roles['Participante'].append(user)
                            approved_list.append(email)
                        elif new_role == 'Negar':
                            if 'Excluir' not in modified_roles:
                                modified_roles['Excluir'] = []
                            modified_roles['Excluir'].append(user)
                            denied_list.append(email)

            for role, users in modified_roles.items():
                setMembersCoPRole(community, users, role)

            moderator_id = portal_membership.getAuthenticatedMember().getId()
            moderator_name, email = getMemberData(community, moderator_id)
            moderator_name = encodeUTF8(moderator_name)
            settings = getCoPSettings()
            mail_from = settings.username
            message = 'Content-Type: text/plain; charset="UTF-8"\nMIME-Version: 1.0\n\n'
            if approved_list:
                subject = encodeUTF8("Participação aprovada")
                message += encodeUTF8("Sua participação na comunidade %s foi aprovada." % (community.Title()))
                message += encodeUTF8('\n\n\nLink para acesso a comunidade:\n%s' % (community.absolute_url()))
                createMessage(mail_from, approved_list, subject, message, self.context.absolute_url())
                sendAsyncMail()

            if denied_list:
                moderator_message = self.request.get('refused_message')
                subject = encodeUTF8("Participação não aprovada")
                message = 'Content-Type: text/plain; charset="UTF-8"\nMIME-Version: 1.0\n\n'
                message += encodeUTF8("Sua participação na comunidade %s não foi aprovada pelo Moderador." % (community.Title()))
                if moderator_message:
                    message += encodeUTF8("\nMotivo:\n%s" % (moderator_message))
                message += encodeUTF8("\n\nMensagem enviada por: %s" % (moderator_name))
                createMessage(mail_from, denied_list, subject, message, self.context.absolute_url())
                sendAsyncMail()

            self.context.plone_utils.addPortalMessage(
                u'As alterações foram salvas', 'info'
            )
            return self.request.response.redirect(
                self.context.absolute_url()
            )

        return None

    def get_filter_terms(self):
        self.filter_terms = [('', 'Pesquisar')]
        return self.filter_terms

    def get_cop_role(self, user_id):
        community = getCoPContext(self.context)
        roles = community.get_local_roles_for_userid(user_id)
        if "Owner" in roles:
            return "Owner"
        return roles[0] if roles else False

    def check_permission(self, permission, cop=False):
        if cop:
            context = getCoPContext(self.context)
        else:
            context = self.context
        return checkPermission(permission, context)

    def can_edit(self):
        return self.check_permission('cmf.ModifyPortalContent', cop=True)

    def can_review_state(self):
        return self.check_permission('cmf.ReviewPortalContent', cop=True)

    def can_moderate(self):
        return self.check_permission('cmf.ReviewPortalContent') or \
               self.context.get_gestao_habilitado() and \
               self.check_permission('communities.practice.ModerateCoP')

    def set_coppla_workflow(self):
        context = aq_parent(self.context)
        self.context_workflow = {}
        self.context_workflow['review_state'] = getReviewStateTitle(context)
        wf_tool = getToolByName(context, 'portal_workflow')
        self.context_workflow['transitions'] = wf_tool.listActionInfos(object=context)
        for transition in self.context_workflow['transitions']:
            transition['description'] = getReviewStateDescription(transition['id'])
        return self.context_workflow

    def get_cop_edit(self):
        context = aq_parent(self.context)
        cop_edit = {
            'title': 'Editar',
            'url':  "%s/edit" % (context.absolute_url()),
        }
        return cop_edit

    def get_leave_cop_url(self):
        community = getCoPContext(self.context)
        leave_url = "%s/viewCoPLeave" % (community.absolute_url())
        return leave_url

    def can_leave_cop(self):
        portal_membership = getToolByName(
            self.context, 'portal_membership'
        )
        authenticated = portal_membership.getAuthenticatedMember()
        user_id = authenticated.getId()
        user_role = self.get_cop_role(user_id)
        community = getCoPContext(self.context)
        super_role = hasSuperRole(community, user_id)
        super_user = super_role or user_role == "Owner"
        return not super_user

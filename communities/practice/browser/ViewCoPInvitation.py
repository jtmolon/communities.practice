# -*- coding: utf-8 -*-
from Products.Five.browser import BrowserView
from Products.CMFCore.utils import getToolByName
from zope.component.hooks import getSite

from communities.practice.generics.generics import getMemberData
from communities.practice.generics.generics import getCommunitiesParticipating
from communities.practice.generics.generics import getCoPContext
from communities.practice.generics.asyncmail import createMessage
from communities.practice.generics.asyncmail import sendAsyncMail
from communities.practice.generics.generics import encodeUTF8
from communities.practice.generics.generics import getCoPSettings


class ViewCoPInvitation(BrowserView):
    """Default View for Communities of Practice"""

    def get_cop_context(self):
        self.cop_context = getCoPContext(self.context, ['CoP'])
        return self.cop_context

    def get_communities_participating(self):
        portal = getSite()
        portal_membership = getToolByName(self.context, 'portal_membership')
        user = portal_membership.getAuthenticatedMember()
        communities = getCommunitiesParticipating(user, portal)
        if not hasattr(self, 'cop_context'):
            self.get_cop_context()
        if self.cop_context:
            communities.remove(self.cop_context)
        return communities

    def send_invitation_request(self):
        form = self.request.form
        if 'submit' in form.keys():
            form.pop('submit')
            message = form.pop("message")
            mail_list = form.pop("email_list")
            mail_list = mail_list.replace(' ', '')
            if mail_list:
                mail_list = mail_list.split(',')
            if form and mail_list:
                self.send_invitation_mail(message, mail_list, form)
            self.context.plone_utils.addPortalMessage(
                u'Seu convite foi enviado.', 'info'
            )
            return self.request.response.redirect(self.context.absolute_url())
        return None

    def send_invitation_mail(self, user_message, mail_list, cop_list):
        """Send message for moderators."""
        portal = getSite()
        portal_membership = getToolByName(self.context, "portal_membership")
        user_id = portal_membership.getAuthenticatedMember().getId()
        user_name = encodeUTF8(getMemberData(self.context, user_id)[0])
        portal_title = encodeUTF8(portal.Title())
        num_cop = len(cop_list)

        settings = getCoPSettings()
        mail_from = settings.username
        message = 'Content-Type: text/html; charset="UTF-8"\nMIME-Version: 1.0\nContent-Transfer-Encoding: 7bit\n\n'
        message += """
                    <html>
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                    </head>
                    <body style="background-color: fff;">
                    <div style="background-color:rgb(242,242,242); border-radius: 0.85em; padding: 20px 20px 40px 20px;">
                    <div width="98%%" border="0" cellspacing="0" style="border:thin solid #d4d4d4;border-radius:0.85em;background-color:#fff;padding: 20px">
                    """
        subject = "%s convidou você a participar do Portal %s" % (user_name, portal_title)
        message += "%s<br>" % (subject)

        if user_message:
            message += "<br>Mensagem deixada por %s:<br> %s <br><br>" % (user_name, user_message)

        message += """A Comunidade de Prática é uma plataforma para a construção de comunidades virtuais de práticas voltadas para aplicações de aprendizagem social. Oferece um conjunto de ferramentas de comunicação e colaboração integrados em um ambiente comum para compartilhar conhecimentos.<br><br>"""
        message += "Cadastre-se no portal <a href=%s>%s</a> <br><br>" % (portal.absolute_url(), portal_title)

        if num_cop == 1:
            message += "Visite a Comunidade:<br>"
            message += "<a href=%s>%s</a><br><br>" % (cop_list.values()[0], cop_list.keys()[0])

        if num_cop > 1:
            message += "Visite as Comunidades:<br>"
            for cop_title, cop_url in cop_list.items():
                message += "<a href=%s>%s</a><br>" % (cop_url, cop_title)

        message += """
                    </div>
                    </div>
                    </body>
                    </html>
                    """
        createMessage(
            mail_from, mail_list, subject,
            message, self.request.getURL())
        sendAsyncMail()

# -*- coding: utf-8 -*-
from Products.CMFCore.utils import getToolByName
from communities.practice.browser.ViewCoPBase import ViewCoPBase
from communities.practice.generics.cache import getCoPLocalRoles
from communities.practice.generics.generics import getMembersData
import re


class ViewCoPMembers(ViewCoPBase):
    """ Default view for search into CoPs.
    """

    def set_context_title(self):
        self.context_title = {}
        self.context_title['title'] = "Participantes"
        self.context_title['description'] = ""
        return self.context_title

    def get_cop_search(self):
        """ Get community members. Filtering by SearchableText.
        """
        members = []
        catalog = getToolByName(self.context, 'portal_catalog')
        brain = catalog(UID=self.context.UID())
        allow_role = ['Moderador', 'Participante']
        for cop_brain in brain:
            membersByRole = getCoPLocalRoles(cop_brain).get(cop_brain.UID)
            for role in allow_role:
                members += membersByRole[role]

        search_term = self.request.get(self.get_search_parameter(), None)
        if search_term:
            filter_list = []
            for member in members:
                filtered = re.search(search_term, member[1], re.I)
                if filtered:
                    filter_list.append(member)
            return filter_list
        return members

    def get_members_data(self, member_list):
        """ Gets members data only for the batch list.
        """
        members_data = []
        members_data += getMembersData(
            self.context, [user[0] for user in member_list]
        )
        return members_data

    def get_search_parameter(self):
        return 'user_search'

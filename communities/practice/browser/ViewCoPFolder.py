# -*- coding: utf-8 -*-
from zope.component import getMultiAdapter

from communities.practice.browser.ViewCoPBase import ViewCoPBase
from communities.practice.generics.generics import canDeleteCoPFolder

class ViewCoPFolder(ViewCoPBase):
    """Default View for CoPFolder"""

    def set_context_actions(self):
        self.context_actions = []
        context_state = getMultiAdapter(
            (self.context, self.request),
            name='plone_context_state'
        )
        if context_state.is_editable():
            self.context_actions.append({
                'id': 'edit',
                'title': 'Edit',
                'url': "%s/edit" % (self.context.absolute_url()),
            })
        edit_actions = context_state.actions('object_buttons')
        can_delete = canDeleteCoPFolder(self.context)
        for action in edit_actions:
            append = action['id'] not in ['delete', 'cut'] or can_delete
            if append:
                self.context_actions.append({
                    'id': action['id'],
                    'title': action['title'],
                    'url': action['url'],
                })
        return self.context_actions

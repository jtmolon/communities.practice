# -*- coding: utf-8 -*-
from Products.CMFCore.utils import getToolByName
from zope.component.hooks import getSite
from communities.practice.generics.generics import getMemberData
from communities.practice.generics.generics import encodeUTF8
from communities.practice.generics.generics import getMMGroupMembers
from communities.practice.generics.generics import getOMGroupMembers
from communities.practice.generics.generics import getCoPSettings
from communities.practice.generics.generics import getLocalRole
from communities.practice.generics.generics import getReviewStateDescription
from communities.practice.generics.generics import getReviewStateTitle
from communities.practice.generics.generics import setMasterRole
from communities.practice.generics.generics import searchPortalUsers
from communities.practice.generics.asyncmail import sendAsyncMail
from communities.practice.generics.asyncmail import createMessage
from communities.practice.subscribers import createMastersGroups
from communities.practice.browser.ViewCoPCommunityFolder import \
    ViewCoPCommunityFolder


class ViewCoPCommunityFolderConfig(ViewCoPCommunityFolder):
    """Default View for Community Folder"""

    def can_edit(self):
        portal_membership = getToolByName(
            self.context, 'portal_membership'
        )
        authenticated = portal_membership.getAuthenticatedMember()
        user_id = authenticated.getId()
        local_role = getLocalRole(self.context, user_id)
        role = authenticated.getRoles()
        return True if "Owner" in local_role or "Manager" in role else False

    def get_filter_terms(self):
        self.filter_terms = [
            ('', 'Pesquisar'),
        ]
        return self.filter_terms

    def set_cop_menu_selected(self, title_menu=""):
        self.cop_menu_selected = 'configuracoes'
        return self.cop_menu_selected

    def set_coppla_workflow(self):
        context = self.context
        self.context_workflow = {}
        self.context_workflow['review_state'] = getReviewStateTitle(context)
        wf_tool = getToolByName(context, 'portal_workflow')
        self.context_workflow['transitions'] = wf_tool.listActionInfos(object=context)
        for transition in self.context_workflow['transitions']:
            transition['description'] = getReviewStateDescription(transition['id'])
        return self.context_workflow

    def get_cop_edit(self):
        cop_edit = {
            'title': 'Editar',
            'url':  "%s/edit" % (self.context.absolute_url()),
        }
        return cop_edit

    def set_context_title(self):
        self.context_title = {}
        self.context_title['title'] = "Configurações"
        self.context_title['description'] = ""
        return self.context_title

    def get_users(self):
        user_config = self.request.get("user_config", None)
        if user_config == "add":
            self.users = self.get_add_moderators()
        else:
            self.users = self.get_moderators()
        return self.users

    def set_users(self):
        user_config = self.request.get("user_config", None)
        if user_config == "add":
            self.users = self.set_add_moderators()
        else:
            self.users = self.set_moderators()
        return self.users

    def get_add_moderators(self):
        """ Retorna os membros do portal que ainda nao sao
            Moderador_Master ou Observador_Master
        """
        search_filter = self.request.get('SearchableText', [])
        users = []
        if len(search_filter) < 1:
            return users

        community_folder = self.context
        portal_groups = getToolByName(community_folder, "portal_groups")
        users = []
        groups_users = []
        mm_group = portal_groups.getGroupById("MM%s" % community_folder.UID())
        om_group = portal_groups.getGroupById("OM%s" % community_folder.UID())
        if not mm_group or not om_group:
            createMastersGroups(community_folder, False)
            mm_group = portal_groups.getGroupById("MM%s" % community_folder.UID())
            om_group = portal_groups.getGroupById("OM%s" % community_folder.UID())
        groups_users += mm_group.getGroupMemberIds()
        groups_users += om_group.getGroupMemberIds()
        users = searchPortalUsers(
            self.context,
            self.request,
            search_filter,
            groups_users
        )

        transitions = ["Selecionar", "Moderador_Master", "Observador_Master"]
        for user in users:
            user["role"] = "Selecionar"
            user["transitions"] = transitions
            user["super"] = False

        return users

    def set_add_moderators(self):
        """ Adiciona os participantes selecionados como
            Moderador_Master ou Observador_Master
            Envia um e-mail notificando que usuario foi inserido
        """
        if self.request.get("update_roles"):
            form_items = self.request.form.items()
            community_folder = self.context
            mail_to_observadores = []
            mail_to_moderadores = []

            for user_id, roles in form_items:
                new_role = roles[0] if isinstance(roles, list) else None
                if new_role in ["Moderador_Master", "Observador_Master"]:
                    setMasterRole(self.context, user_id, new_role)
                    member_data = getMemberData(community_folder, user_id)
                    if member_data:
                        user_name, email = member_data
                        if new_role == "Observador_Master":
                            mail_to_observadores.append(email)
                        else:
                            mail_to_moderadores.append(email)

            mail_lists = (
                (mail_to_observadores, "Observador"),
                (mail_to_moderadores, "Moderador Master"),
            )
            settings = getCoPSettings()
            mail_from = settings.username
            for mail_to, role in mail_lists:
                if mail_to:
                    subject = encodeUTF8("%s em Pasta" % role)
                    message = encodeUTF8(
                        "Você foi adicionado como %s na pasta %s.\n" % (
                            role, community_folder.Title()
                        )
                    )
                    message += encodeUTF8(
                        "Para acessar a pasta clique em %s" % (
                            community_folder.absolute_url()
                        )
                    )
                    createMessage(
                        mail_from, mail_to, subject,
                        message, self.request.getURL()
                    )
                    sendAsyncMail()

            url = "%s/viewCoPCommunityFolderConfig#tab_cop_moderadores" % (
                self.context.absolute_url()
            )
            portal = getSite()
            portal.plone_utils.addPortalMessage(
                u'Os novos usuários foram adicionados', 'info'
            )
            portal.REQUEST.RESPONSE.redirect(url)

        return ""

    def get_moderators(self):
        """ Retorna os membros dos grupos Moderador_Master ou Observador_Master
        """
        portal = getSite()
        portal_url = portal.absolute_url()
        community_folder = self.context
        master_users = []

        groups = (
            (getMMGroupMembers(self.context), "Moderador_Master"),
            (getOMGroupMembers(self.context), "Observador_Master"),
        )
        transitions = ['Moderador_Master', 'Observador_Master', 'Excluir']

        for group_members, master_role in groups:
            for user_id in group_members:
                member_data = getMemberData(community_folder, user_id)
                if member_data:
                    user_name = member_data[0]
                    master_users.append({
                        "full_name": user_name.capitalize(),
                        "id": user_id,
                        "role": master_role,
                        "transitions": transitions,
                        "author_url": "%s/author/%s" % (portal_url, user_id),
                        "super": False,
                    })
        master_users.sort(key=lambda item: item['full_name'])
        return master_users

    def set_moderators(self):
        """ Altera o status da participacao master local e
            das comunidades recursivamente.
            Form retorna dicionario com id do usuario como chave e uma lista
            com o papel atual e o papel anterior do usuario como valor.
            Caso as permissoes sejam diferentes,
            novo papel e' atribuido ao usuario.
        """
        if self.request.get('update_roles'):
            master_users = [user['id'] for user in self.get_moderators()]
            form_items = self.request.form.items()
            for user, roles in form_items:
                new_role = roles[0] if isinstance(roles, list) else None
                old_role = roles[1] if isinstance(roles, list) else None
                if new_role != old_role and user in master_users:
                    setMasterRole(self.context, user, new_role)
            portal = getSite()
            url = self.context.absolute_url() + "/viewCoPCommunityFolderConfig"
            portal.plone_utils.addPortalMessage(
                u'As alterações foram salvas', 'info'
            )
            portal.REQUEST.RESPONSE.redirect(url)
        return None

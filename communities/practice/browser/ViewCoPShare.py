# -*- coding: utf-8 -*-
from Products.CMFPlone.utils import getToolByName

from communities.practice.browser.ViewCoPBase import ViewCoPBase


class ViewCoPShare(ViewCoPBase):
    """Default View for CoPShare"""

    def get_parent_url(self):
        parent_url = ''
        ct = getToolByName(self.context, 'portal_catalog')
        parent = ct(UID=self.context.getParent_uid())
        if parent:
            parent_url = '%s/view' % parent[0].getURL()
        return parent_url

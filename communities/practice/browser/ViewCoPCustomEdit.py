# -*- coding:utf8 -*-
import itertools

from Acquisition import aq_base
from Products.Archetypes.event import ObjectEditedEvent
from Products.Archetypes.browser.edit import Edit

from communities.practice.browser.ViewCoPBase import ViewCoPBase
from communities.practice.generics.generics import getProductInstalled
from communities.practice.config import INPUTS_FORM_IDS
from communities.practice.config import OPCOES
from communities.practice.subscribers import editionCreatedContents

class ViewCoPCustomEdit(ViewCoPBase, Edit):
    """Default View for edit Communities of Practice
    """

    def set_context_title(self):
        self.context_title = {}
        self.context_title['title'] = 'Edição da Comunidade'
        self.context_title['description'] = ''
        return self.context_title

    def cop_forms_installed(self):
        return getProductInstalled('cop.forms')

    def set_tools(self):
        #obj = aq_base(self.context)
        obj = self.context
        self.tools = []
        titlemenus = [
            'acervo', 'calendario', 'forum',
            'portfolio', 'tarefas', 'subcop',
        ]
        zipped = itertools.izip(INPUTS_FORM_IDS[:6], titlemenus)
        for tool_id, titlemenu in zipped:
            tool_dict = {}
            field = obj.Schema()[tool_id]
            widget = field.widget
            tool_dict['id'] = tool_id
            tool_dict['title'] = widget.label
            tool_dict['titlemenu'] = titlemenu
            tool_dict['checked'] = getattr(
                obj, tool_id, False
            ) == OPCOES[0]
            self.tools.append(tool_dict)
        return self.tools

    def update_data(self):
        #obj = aq_base(self.context)
        obj = self.context
        form = self.request.form
        if 'submit_coppla_edit' in form.keys():
            title = form.get('title', '')
            description = form.get('description', '')
            subject_keywords = form.get('subject_keywords', [])
            subject_existing_keywords = \
                form.get('subject_existing_keywords', [])
            subject = set(subject_keywords + subject_existing_keywords)
            subject = tuple(subject)
            participar_input = form.get('participar_input', 'Desabilitar')
            available_forms = form.get('available_forms', [])
            available_forms = tuple(available_forms)

            obj.setTitle(title)
            obj.setDescription(description)
            obj.setSubject(subject)
            obj.setParticipar_input(participar_input)
            obj.setAvailable_forms(available_forms)

            # set tools values
            for field_id in INPUTS_FORM_IDS[:6]:
                value = OPCOES[0] if field_id in form else OPCOES[1]
                setattr(obj, field_id, value)

            obj.reindexObject()
            editionCreatedContents(obj, False)
            return self.request.response.redirect(obj.absolute_url())

        return False

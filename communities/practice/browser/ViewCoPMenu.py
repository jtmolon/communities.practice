from communities.practice.browser.ViewCoPBase import ViewCoPBase
from communities.practice.generics.generics import getCoPContext
from Products.CMFPlone.utils import getToolByName


class ViewCoPMenu(ViewCoPBase):
    """Default View for CoPMenu"""

    def post_redirection(self):
        """ Redirection whether context is Posts CoPMenu.
        """
        if getattr(self.context, 'titlemenu', '') == 'posts':
            cop_context = getCoPContext(self.context)
            return self.request.response.redirect(cop_context.absolute_url())
        return False

    def get_addable_types(self):
        """ Esta funcao so' esta' aqui para alterar a ordem entre ata e evento no calendario.
            Com o novo layout este requisito pode ser revisto
        """
        self.addable_types = super(ViewCoPMenu, self).get_addable_types()
        if getattr(self.context, 'titlemenu', '') == 'calendario':
            self.addable_types.reverse()
        return self.addable_types

# -*- coding: utf-8 -*-
from communities.practice.browser.ViewCoPBase import ViewCoPBase
from communities.practice.generics.asyncmail import sendAsyncMail
from communities.practice.generics.asyncmail import createMessage
from communities.practice.generics.generics import allowTypes
from communities.practice.generics.generics import encodeUTF8
from communities.practice.generics.generics import generateContentId
from communities.practice.generics.generics import getCoPSettings
from communities.practice.generics.generics import getMemberData
from communities.practice.generics.generics import getUsersByRole
from communities.practice.generics.generics import getCoPContext
from Products.CMFPlone.utils import _createObjectByType
from Products.CMFPlone.utils import getToolByName


class ViewCoPNotification(ViewCoPBase):
    """Default view para notificacoes.
    """

    def send_message_members(self):
        """Envia mensagem para os participantes da comunidade.
        """
        form = self.request.form
        if 'submit_send_message_members' in form.keys():
            portal_membership = getToolByName(
                self.context, 'portal_membership'
            )
            authenticated = portal_membership.getAuthenticatedMember()
            member_data = getMemberData(self.context, authenticated.getId())
            moderador_nome = encodeUTF8(member_data[0])
            community = getCoPContext(self.context)
            url = encodeUTF8(community.absolute_url())

            assunto = encodeUTF8(form.get("assunto"))
            mensagem = form.get("mensagem")
            if assunto and mensagem:
                email_mensagem = 'Content-Type: text/plain;'
                email_mensagem += 'charset="UTF-8"\nMIME-Version: 1.0\n\n'
                email_mensagem += encodeUTF8(mensagem)
                email_mensagem += \
                    '\n\n\nLink para acesso a comunidade:\n%s' % (url)
                email_mensagem += \
                    '\n\nMensagem enviada por: %s' % (moderador_nome)

                mail_to = []
                for user_id, valor in form.items():
                    if valor == 'Sim':
                        mail = getMemberData(community, user_id)[1]
                        if mail:
                            mail_to.append(mail)
                if mail_to:
                    settings = getCoPSettings()
                    mail_from = settings.username
                    createMessage(
                        mail_from, mail_to, assunto,
                        email_mensagem, self.context.absolute_url()
                    )
                    sendAsyncMail()
                    self.store_notification(
                        mail_from, mail_to, assunto, mensagem
                    )
                    status_message = u'Email enviado com sucesso.'
                    message_type = 'info'
                else:
                    status_message = u"Selecione emails para envio."
                    message_type = 'error'
            else:
                status_message = u"Campos obrigatórios não preenchidos"
                message_type = 'error'
            self.context.plone_utils.addPortalMessage(
                status_message, message_type
            )
            return self.request.response.redirect(
                self.get_action_url()
            )
        return ""

    def get_members(self):
        community = getCoPContext(self.context)
        tipos = getUsersByRole(
            community, ['Participante', 'Moderador']
        )
        mail_to = []
        for papel in ['Participante', 'Moderador']:
            for participante in tipos[papel]:
                mail_to.append({
                    'id': participante['id'],
                    'nome': participante['full_name'],
                })
        mail_to.sort(key=lambda item: item['nome'])
        return mail_to

    def store_notification(self, mail_from, mail_to, subject, message):
        """Creates CoPDocument when message is sent."""
        allowTypes(self.context, ['CoPFile'])
        notification = _createObjectByType(
            'CoPDocument',
            self.context,
            id=generateContentId(self.context, 'message'),
            title=subject,
        )
        notification.setTitle(subject)
        notification.setText(
            "%s\n\n%s" % (', '.join(mail_to), message)
        )
        notification.unmarkCreationFlag()
        portal_workflow = getToolByName(self.context, "portal_workflow")
        portal_workflow.doActionFor(notification, 'privado')
        notification.reindexObject()
        allowTypes(self.context, [])

    def get_action_url(self):
        """ Returns form action URL
        """
        return self.context.absolute_url()

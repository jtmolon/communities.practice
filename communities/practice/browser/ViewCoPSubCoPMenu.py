# -*-coding:utf-8-*-
from communities.practice.browser.ViewCoPBase import ViewCoPBase
from communities.practice.browser.ViewCoPCommunityFolder import \
    ViewCoPCommunityFolder


class ViewCoPSubCoPMenu(ViewCoPCommunityFolder):
    """Default View for CoPMenu SubCoP"""

    def set_cop_menu(self):
        self.cop_menu = ViewCoPBase.set_cop_menu(self)
        return self.cop_menu

# -*- coding: utf-8 -*-
from communities.practice.browser.ViewCoPCommunityFolder import \
    ViewCoPCommunityFolder
from communities.practice.generics.atividade_generics import \
    exportCoPCommunityFolderAtividade, \
    getCoPAtividadeTotalTypes, \
    getCoPCommunityFolderAtividadeTypes, \
    getCoPAtividadeCommunities, \
    getStartEndDate
from zope.security import checkPermission


class ViewCoPCommunityFolderActivity(ViewCoPCommunityFolder):
    """Default view for users content.
    """

    def set_cop_menu_selected(self, title_menu=""):
        self.cop_menu_selected = 'copcommunityfolderatividade'
        return self.cop_menu_selected

    def set_context_title(self):
        self.context_title = {}
        self.context_title['title'] = "Atividade"
        self.context_title['description'] = \
            "Resumo das atividades nas comunidades"
        return self.context_title

    def get_types(self):
        types = getCoPCommunityFolderAtividadeTypes()
        return types

    def check_permission(self):
        return checkPermission('cmf.ReviewPortalContent', self.context)

    def get_communities(self):
        """Retorna os brains das comunidades do folder
        """
        return getCoPAtividadeCommunities(self.context)

    def get_total_types(self):
        """Retorna o total de cada tipo de conteudo da comunidade
        """
        communities_content = []
        communities = self.get_communities()
        period = getStartEndDate(self.request.form)
        for community in communities:
            total_content = getCoPAtividadeTotalTypes(
                community["brain"], "copcommunityfolder", period
            )
            if total_content.get(("total", u"Total"), False):
                community_content = {}
                community_content["community"] = community
                community_content["total_content"] = total_content
                communities_content.append(community_content)
        return communities_content

    def export_data(self):
        """Exporta os dados da view
        """
        form = self.request.form
        if "submit_export_atividades" in form.keys():
            exportCoPCommunityFolderAtividade(self.request, self.context)
        return False

# -*- coding: utf-8 -*-
from plone.app.registry.browser.controlpanel import RegistryEditForm
from plone.app.registry.browser.controlpanel import ControlPanelFormWrapper
from z3c.form import form
from z3c.form.browser.checkbox import CheckBoxFieldWidget

from communities.practice import practiceMessageFactory as _
from communities.practice.interfaces import ICoPSettings


class CoPSettingsControlPanelForm(RegistryEditForm):
    form.extends(RegistryEditForm)
    schema = ICoPSettings
    control_panel_view = "@@coppla-settings"

    def updateFields(self):
        super(CoPSettingsControlPanelForm, self).updateFields()
        self.fields['available_forms'].widgetFactory = \
            CheckBoxFieldWidget

    def updateWidgets(self):
        super(CoPSettingsControlPanelForm, self).updateWidgets()


class CoPSettingsControlPanel(ControlPanelFormWrapper):
    form = CoPSettingsControlPanelForm
    label = _(u"CoPPla settings")
    description = _(u"Communities of Practice Platforms settings")

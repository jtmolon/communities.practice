require.config({
    // app entry point
    shim: {
        'bootstrap/affix':      { deps: ['jquery'], exports: '$.fn.affix' }, 
        'bootstrap/alert':      { deps: ['jquery'], exports: '$.fn.alert' },
        'bootstrap/button':     { deps: ['jquery'], exports: '$.fn.button' },
        'bootstrap/carousel':   { deps: ['jquery'], exports: '$.fn.carousel' },
        'bootstrap/collapse':   { deps: ['jquery'], exports: '$.fn.collapse' },
        'bootstrap/dropdown':   { deps: ['jquery'], exports: '$.fn.dropdown' },
        'bootstrap/modal':      { deps: ['jquery'], exports: '$.fn.modal' },
        'bootstrap/popover':    { deps: ['jquery'], exports: '$.fn.popover' },
        'bootstrap/scrollspy':  { deps: ['jquery'], exports: '$.fn.scrollspy' },
        'bootstrap/tab':        { deps: ['jquery'], exports: '$.fn.tab'        },
        'bootstrap/tooltip':    { deps: ['jquery'], exports: '$.fn.tooltip' },
        'bootstrap/transition': { deps: ['jquery'], exports: '$.fn.transition' },
        "coppla": { deps: ['jquery']}
    },
    paths: {
        /* path to folder where individual bootstrap files have been saved. (affix.js, alert.js, etc) */
        'bootstrap': 'bootstrap',
        /* jQuery from CDN  */
        'jquery': 'jquery'
    },
    map: {
        '*': { 'jquery': 'jquery-private' },
        'jquery-private': { 'jquery': 'jquery' },
    },
});

require([
         'jquery-private',
         'bootstrap/dropdown',
         "bootstrap/affix",
         "bootstrap/modal",
         "coppla/CoPEdit.js",
         "coppla/CoPPostForm.js",
         "coppla/CoPCommunities.js",
         "coppla/CoPTimeline.js",
         "coppla/CoPMessageMembers.js",
         "coppla/CoPSearch.js",
         "coppla/CoPModal.js",
         "coppla/CoPShare.js",
         "coppla/CoPAddableTypes.js",
         "coppla/CoPAjaxLoad.js",
         "coppla/CoPConfig.js",
         "coppla/CoPMenuMobile",
         "coppla/CoPDiscussion.js",
         "coppla/CoPColorSelect.js"

         ], function($){
         $(window).trigger("load");
});

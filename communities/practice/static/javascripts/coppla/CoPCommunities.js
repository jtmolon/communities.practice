$(document).on("click", ".cop-sub-cop-source a.cop-sub-cop-url", function(e){
    e.preventDefault();
    var parent_cop = $(this).closest(".cop-listing-item");
    var container = $(this).closest(".cop-listing-item").find(".cop-sub-cop-container");
    if(container.find(".cop-communities-list").length == 0){
        var cop_url = $(this).attr("href");
        var params = window.location.href.split("?")[1];
        if(params){
            filter_term = params.match("filter_term=[^&#=]*");
            if (filter_term){
                cop_url = cop_url + "?"+ filter_term;
            }
        }
        container.load(cop_url + " .cop-communities-list", function(data){
            if(!parent_cop.hasClass('cop-even')){
                $(this).find('.cop-listing-item').addClass('cop-even');
            }
        });
    }
    else{
        container.find('.cop-communities-list').first().toggleClass('cop-hide-communities');
    }

    // Seta subcomunidades//
    var span_icon = $(this).find('span:eq(0)');
    span_icon.toggleClass('cop-arrow cop-right');
    span_icon.toggleClass('cop-arrow cop-down');
});

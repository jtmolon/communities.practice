function CopClassColor(){
    value = $('select#color').val();
    element = $('#archetypes-fieldname-color .formQuestion');
    select = $('select#color');
    element.addClass("cop-color-"+value);
    select.addClass("cop-selct-image-"+value);
}

$(document).on(
    'change keyup keydown',
    'select#color', function() {
    color = $('select#color').val();

    item = $("[class*='cop-color-']")
    classes = item.first().attr('class')
    item.attr('class', classes.replace(/cop-color-.*/, ''))
    $('#archetypes-fieldname-color .formQuestion').addClass("cop-color-"+color);

    item_select = $("[class*='cop-selct-image-']")
    classes_select = item_select.first().attr('class')
    item_select.attr('class', classes_select.replace(/cop-selct-image-.*/, ''))
    $('select#color').addClass("cop-selct-image-"+color);
});
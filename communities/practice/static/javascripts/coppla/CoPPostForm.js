$(document).on('click','.cop-post-creation #submit_cop_post', function(e){
    e.preventDefault();
    if($('.cop-post-creation #post_message').val()){
        form = $(this).closest('form');
        str = form.serialize();
        $.post('viewCoP', str, function(data){
            post_uid = $(data).find('.cop-post-creation #post_uid');
            post_uid = post_uid.attr('value');
            url = $(this.url).context.URL;
            current_url = url.split('?')[0];
            params = url.split('?')[1];
            var regex = new RegExp('/view/*$');
            current_url = current_url.replace(regex, '');
            $('.cop-timeline-news-list').prepend(
                '<div style="display:none;"id= "' + post_uid + '"></div>'
            );
            if(!params){
                $('.cop-timeline-news-list #' + post_uid).load(
                    current_url + '/viewCoPSearch?UID=' + post_uid +
                    ' #' + post_uid, function(){
                        $('#'+post_uid).find('.cop-timeline-news-item').append('<hr>');
                    }
                );
                $('.cop-post-creation #post_message').val('');
                $('.cop-timeline-news-list #' + post_uid).fadeIn(2000);
                $('#post_length').html('600');
            }
            else{
                $('.cop-contents').load(
                    current_url + '/view .cop-contents > *', function(){
                        $('.cop-timeline-news-list #' + post_uid).hide();
                        $('.cop-timeline-news-list #' + post_uid).fadeIn(2000);
                    }
                );
            }
        });
    }
});

$(document).delegate('#post_message', 'keyup', function(){
    var max_length = $(this).attr('maxlength');
    remaining = max_length - $(this).val().length;
    $('#post_length').html(remaining);
});

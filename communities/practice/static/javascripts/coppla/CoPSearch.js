$(document).on("click", ".cop-searchbox li a", function(e){
    e.preventDefault();
    var selected_text = $(this).text();
    var span_title = $(this).closest('.cop-input-group-btn').find(
      '.cop-selected-text'
    );
    span_title.html(selected_text);
    filter_term = this.getAttribute('id');
    var filter_term_input = $(this).closest('.cop-input-group-btn').find(
        'input[name=filter_term]'
    );
    filter_term_input.val(filter_term);

    form = $(this).closest("form[name=copsearchform]");
    str = form.serialize();
    $.post("", str, function(data){
        var new_url = window.location.protocol + "//" +
            window.location.host + window.location.pathname + '?' + str;
        window.history.pushState({path:new_url},'',new_url);
        $(".cop-contents").replaceWith($(data).find('.cop-contents'));
    });
});

$(document).on("submit", "form[name=copsearchform]", function(e){
    e.preventDefault();
    var form = $(this);
    var str = form.serialize();
    var post_url = window.location.protocol + "//" +
        window.location.host + window.location.pathname;
    var new_url = post_url + '?' + str;
    $.post(post_url, str, function(data){
        window.history.pushState({path:new_url},'',new_url);
        $(".cop-contents").replaceWith($(data).find('.cop-contents'));
        //somente para pesquisa de usuários nas configurações
        if($('.cop-panel#users').length){
            $('html,body').animate({scrollTop: $('.cop-panel#users').offset().top}, 100);
        }
    });
});

({
    baseUrl: "javascripts",
    paths: {
        requireLib: "require"
    },
    include: "requireLib",
    name: "main",
    out: "../browser/javascripts/coppla.js",
    optimize: "none"
})

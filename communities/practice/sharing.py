from zope.interface import implements
from plone.app.workflow.interfaces import ISharingPageRole
from Products.CMFPlone import PloneMessageFactory as _

class Aguardando(object):
   implements(ISharingPageRole)
   title = _(u"Aguardando", default=u"Aguardando")
   required_permission = 'View'

class Bloqueado(object):
   implements(ISharingPageRole)
   title = _(u"Bloqueado", default=u"Bloqueado")
   required_permission = 'View'

class Moderador(object):
   implements(ISharingPageRole)
   title = _(u"Moderador", default=u"Moderador")
   required_permission = 'Modify portal content'
   
class Moderador_Master(object):
   implements(ISharingPageRole)
   title = _(u"Moderador_Master", default=u"Moderador_Master")
   required_permission = 'Modify portal content'

class Participante(object):
   implements(ISharingPageRole)
   title = _(u"Participante", default=u"Participante")
   required_permission = 'Modify portal content'

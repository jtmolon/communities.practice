# -*- coding: utf-8 -*-

from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import *
from zope.interface import implements

from Products.CMFDynamicViewFTI.browserdefault import BrowserDefaultMixin

from Products.Archetypes.atapi import BaseContent
from Products.Archetypes.atapi import BaseSchema

from communities.practice.interfaces import ICoPPost
from communities.practice.config import PROJECTNAME


schema = Schema((

),
)

CoPPost_schema = BaseSchema.copy() + \
    schema.copy()

CoPPost_schema['description'].required = 1
CoPPost_schema['description'].schemata = 'default'
CoPPost_schema['description'].widget.maxlength=600
CoPPost_schema['title'].widget.visible = False

for field in ['creators','contributors','allowDiscussion']:
    CoPPost_schema[field].write_permission = "ManagePortal"

class CoPPost(BaseContent, BrowserDefaultMixin):
    """
    """
    security = ClassSecurityInfo()
    implements(ICoPPost)

    meta_type = 'CoPPost'
    _at_rename_after_creation = True

    schema = CoPPost_schema

    # Methods
    def Title(self):
        return "Post"

registerType(CoPPost, PROJECTNAME)

# -*- coding: utf-8 -*-

from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import *
from zope.interface import implements
from communities.practice.interfaces import ICoPPortfolio

from Products.CMFDynamicViewFTI.browserdefault import BrowserDefaultMixin

from Products.ATContentTypes.content.folder import ATFolder
from Products.ATContentTypes.content.folder import ATFolderSchema
from communities.practice.config import PROJECTNAME
from Products.CMFCore.utils import getToolByName
from communities.practice.generics.generics import getMemberData

from Products.validation import validation
from validators import DuplicadoIdValidator
validation.register(DuplicadoIdValidator('isDuplicadoId'))

schema = Schema((
    StringField(
        name='title',
        widget=StringField._properties['widget'](
            label=u'Titulo',
            label_msgid='CoPPortfolio_label_title',
            i18n_domain='communities.practice',
        ),  
        required=1,
        accessor="Title",
        validators = ("isDuplicadoId",),
    ),
),
)

CoPPortfolio_schema = ATFolderSchema.copy() + \
    schema.copy()

for field in ['creators','contributors','excludeFromNav','allowDiscussion', 'nextPreviousEnabled']:
    CoPPortfolio_schema[field].write_permission = "ManagePortal"

class CoPPortfolio(ATFolder):
    """
    """
    security = ClassSecurityInfo()
    implements(ICoPPortfolio)

    meta_type = 'CoPPortfolio'
    _at_rename_after_creation = True

    schema = CoPPortfolio_schema
    
    #Esconder o campo description
    DescriptionField = schema['description']
    DescriptionField.widget.visible = {'edit': 'invisible', 'view': 'invisible'}

    def Title(self):
        #Altera o campo default do campo title
        try:
            member_fullname = getMemberData(self, self.Creator())[0]
        except:
            member_fullname = ""
        if member_fullname:
            return member_fullname
 
registerType(CoPPortfolio, PROJECTNAME)


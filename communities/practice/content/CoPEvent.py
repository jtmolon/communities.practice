# -*- coding: utf-8 -*-

from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import *
from zope.interface import implements

from Products.CMFDynamicViewFTI.browserdefault import BrowserDefaultMixin

from Products.ATContentTypes.content.event import ATEvent
from Products.ATContentTypes.content.event import ATEventSchema

from communities.practice.interfaces import ICoPEvent
from communities.practice.config import *

schema = Schema((

),
)

CoPEvent_schema = ATEventSchema.copy() + \
    schema.copy()

for field in ['creators','contributors','excludeFromNav','allowDiscussion']:
    CoPEvent_schema[field].write_permission = "ManagePortal"

class CoPEvent(ATEvent):
    """
    """
    security = ClassSecurityInfo()
    implements(ICoPEvent)

    meta_type = 'CoPEvent'
    _at_rename_after_creation = True

    schema = CoPEvent_schema


registerType(CoPEvent, PROJECTNAME)

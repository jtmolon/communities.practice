# -*- coding:utf-8 -*-
import atexit
import logging
import os
import smtplib
import stat
import time

from email.header import Header

from zope.sendmail.maildir import Maildir
from zope.sendmail.mailer import SMTPMailer
from zope.sendmail.queue import QueueProcessorThread

from communities.practice.generics import asyncmail_config
from communities.practice.generics.generics import getCoPSettings

import sys
if sys.platform == 'win32':
    import win32file
    _os_link = lambda src, dst: win32file.CreateHardLink(dst, src, None)
else:
    _os_link = os.link

MAX_SEND_TIME = 60*60*3

class queueProcess(QueueProcessorThread):
    """Classe herda de QueueProcessorThread para implementar as seguintes funcoes:
       Salvar log em arquivo, diferente da superclasse que apenas exibe log de erro no terminal
       Encerrar thread apos envio dos email que estao na fila
    """

    def _parseMessage(self, message):
        """Extract fromaddr and toaddrs from the first two lines of
        the `message`.

        Returns a fromaddr string, a toaddrs tuple and the message
        string.
        """

        fromaddr = ""
        toaddrs = ()
        origin = ""
        rest = ""

        try:
            first, second, origin, rest = message.split('\n', 3)
        except ValueError:
            return fromaddr, toaddrs, origin, message

        if first.startswith("X-Zope-From: "):
            i = len("X-Zope-From: ")
            fromaddr = first[i:]

        if second.startswith("X-Zope-To: "):
            i = len("X-Zope-To: ")
            toaddrs = tuple(second[i:].split(", "))

        return fromaddr, toaddrs, origin, rest

    def run(self, forever=False):
        #configuracao do log
        if not self.log.handlers:
            directory = asyncmail_config.DIRECTORY
            logger = directory + '/logger.info'
            file_log = logging.FileHandler(logger)
            file_log.setLevel(logging.INFO)
            formatter = logging.Formatter(fmt='%(asctime)s : %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
            file_log.setFormatter(formatter)
            #seta arquivo de log e verbose
            self.log.addHandler(file_log)
            self.log.setLevel(logging.INFO)

        #chama run da superclasse especificando forever=False
        #para terminar thread apos execucao
        atexit.register(self.stop)
        while not self._stopped:
            for filename in self.maildir:
                # if we are asked to stop while sending messages, do so
                if self._stopped:
                    break

                fromaddr = ''
                toaddrs = ()
                head, tail = os.path.split(filename)
                tmp_filename = os.path.join(head, '.sending-' + tail)
                rejected_filename = os.path.join(head, '.rejected-' + tail)
                try:
                    # perform a series of operations in an attempt to ensure
                    # that no two threads/processes send this message
                    # simultaneously as well as attempting to not generate
                    # spurious failure messages in the log; a diagram that
                    # represents these operations is included in a
                    # comment above this class
                    try:
                        # find the age of the tmp file (if it exists)
                        age = None
                        mtime = os.stat(tmp_filename)[stat.ST_MTIME]
                        age = time.time() - mtime
                    except OSError, e:
                        if e.errno == 2: # file does not exist
                            # the tmp file could not be stated because it
                            # doesn't exist, that's fine, keep going
                            pass
                        else:
                            # the tmp file could not be stated for some reason
                            # other than not existing; we'll report the error
                            raise

                    # if the tmp file exists, check it's age
                    if age is not None:
                        try:
                            if age > MAX_SEND_TIME:
                                # the tmp file is "too old"; this suggests
                                # that during an attemt to send it, the
                                # process died; remove the tmp file so we
                                # can try again
                                os.unlink(tmp_filename)
                            else:
                                # the tmp file is "new", so someone else may
                                # be sending this message, try again later
                                continue
                            # if we get here, the file existed, but was too
                            # old, so it was unlinked
                        except OSError, e:
                            if e.errno == 2: # file does not exist
                                # it looks like someone else removed the tmp
                                # file, that's fine, we'll try to deliver the
                                # message again later
                                continue

                    # now we know that the tmp file doesn't exist, we need to
                    # "touch" the message before we create the tmp file so the
                    # mtime will reflect the fact that the file is being
                    # processed (there is a race here, but it's OK for two or
                    # more processes to touch the file "simultaneously")
                    try:
                        os.utime(filename, None)
                    except OSError, e:
                        if e.errno == 2: # file does not exist
                            # someone removed the message before we could
                            # touch it, no need to complain, we'll just keep
                            # going
                            continue

                    # creating this hard link will fail if another process is
                    # also sending this message
                    try:
                        #os.link(filename, tmp_filename)
                        _os_link(filename, tmp_filename)
                    except OSError, e:
                        if e.errno == 17: # file exists, *nix
                            # it looks like someone else is sending this
                            # message too; we'll try again later
                            continue
                    except Exception, e:
                        if e[0] == 183 and e[1] == 'CreateHardLink':
                            # file exists, win32
                            continue

                    # read message file and send contents
                    file = open(filename)
                    message = file.read()
                    file.close()
                    fromaddr, toaddrs, origin, message = self._parseMessage(message)
                    # The next block is the only one that is sensitive to
                    # interruptions.  Everywhere else, if this daemon thread
                    # stops, we should be able to correctly handle a restart.
                    # In this block, if we send the message, but we are
                    # stopped before we unlink the file, we will resend the
                    # message when we are restarted.  We limit the likelihood
                    # of this somewhat by using a lock to link the two
                    # operations.  When the process gets an interrupt, it
                    # will call the atexit that we registered (``stop``
                    # below).  This will try to get the same lock before it
                    # lets go.  Because this can cause the daemon thread to
                    # continue (that is, to not act like a daemon thread), we
                    # still use the _stopped flag to communicate.
                    self._lock.acquire()
                    try:
                        try:
                            self.mailer.send(fromaddr, toaddrs, message)
                        except smtplib.SMTPResponseException, e:
                            if 500 <= e.smtp_code <= 599:
                                # permanent error, ditch the message
                                self.log.error(
                                    "Discarding email from %s to %s due to"
                                    " a permanent error: %s. Origin: %s.",
                                    fromaddr, ", ".join(toaddrs), str(e), origin)
                                #os.link(filename, rejected_filename)
                                _os_link(filename, rejected_filename)
                            else:
                                # Log an error and retry later
                                raise
                        except smtplib.SMTPRecipientsRefused, e:
                            # All recipients are refused by smtp
                            # server. Dont try to redeliver the message.
                            self.log.error("Email recipients refused: %s. Origin: %s",
                                           ', '.join(e.recipients), origin)
                            _os_link(filename, rejected_filename)

                        try:
                            os.unlink(filename)
                        except OSError, e:
                            if e.errno == 2: # file does not exist
                                # someone else unlinked the file; oh well
                                pass
                            else:
                                # something bad happend, log it
                                raise
                    finally:
                        self._lock.release()
                    try:
                        os.unlink(tmp_filename)
                    except OSError, e:
                        if e.errno == 2: # file does not exist
                            # someone else unlinked the file; oh well
                            pass
                        else:
                            # something bad happend, log it
                            raise

                    # TODO: maybe log the Message-Id of the message sent
                    self.log.info("Mail from %s to %s sent. Origin: %s",
                                  fromaddr, ", ".join(toaddrs), origin)
                    # Blanket except because we don't want
                    # this thread to ever die
                except:
                    if fromaddr != '' or toaddrs != ():
                        self.log.error(
                            "Error while sending mail from %s to %s.",
                            fromaddr, ", ".join(toaddrs), exc_info=True)
                    else:
                        self.log.error(
                            "Error while sending mail : %s ",
                            filename, exc_info=True)
            else:
                if forever:
                    time.sleep(self.interval)

            # A testing plug
            if not forever:
                break



def sendAsyncMail():
    maildir = configureMaildir()
    mailer = configureSMTPMailer()

    queue = queueProcess()
    queue.setMaildir(maildir)
    queue.setMailer(mailer)
    queue.start()


def configureMaildir():
    """Le diretorio do arquivo asyncmail_config.py
       Retorna objeto tipo sendmail.maildir.Maildir
    """
    if not asyncmail_config.DIRECTORY:
        asyncmail_config.DIRECTORY = '/tmp/digestmail'
    directory = asyncmail_config.DIRECTORY
    if not os.path.exists(directory):
    #Caso nao exista o diretorio especificado, parametro 'True' e passado por parametro
    #sendmail.maildir.Maildir cria diretorio e suas subpastas (cur, new, tmp)
        maildir = Maildir(directory,True)
    else:
    #Caso diretorio ja exista, sao criadas pastas cur,new,tmp.
    #Apos sendmail.maildir.Maildir recebe o diretorio pronto.
        if not os.path.exists(directory + '/cur'):
            os.makedirs(directory + '/cur')
        if not os.path.exists(directory +'/new'):
            os.makedirs(directory + '/new')
        if not os.path.exists(directory + '/tmp'):
            os.makedirs(directory + '/tmp')
        maildir = Maildir(directory)
    return maildir


def configureSMTPMailer():
    """Le do control panel as configuracoes de SMTP,
       retorna objeto do tipo sendmail.mailer.SMTPMailer
    """
    settings = getCoPSettings()
    hostname = settings.hostname
    port = settings.port
    username = settings.username
    password = settings.password
    force_tls = settings.force_tls

    mailer = SMTPMailer(hostname=hostname, port=port, username=username,
                        password=password, no_tls=False, force_tls=force_tls)
    return mailer


def createMessage(mail_from, list_mail_to, subject, message, origin=''):
    """Cria email compativel com zope.sendmail
        mail_from recebe string com email de origem
            ex. 'plone@plone.com'
        list_mail_to recebe lista de strings com email dos destinatarios
            ex. ['plone@plone.com', 'plone@plone.com']
        subject recebe string com titulo do email
        message recebe string com corpo do email
    """
    subject = Header(subject, 'utf-8')
    email_message = 'Subject: %s\n%s\n'%(subject, message)
    mail_from = 'X-Zope-From: %s\n' %(mail_from)
    mail_to = 'X-Zope-To: '
    for email in list_mail_to:
        mail_to += '%s, '%(email)
    mail_to += '\n'
    origin += '\n'

    mail_dir = configureMaildir()
    new_message = mail_dir.newMessage()
    new_message.writelines([mail_from, mail_to, origin, email_message])
    new_message.commit()

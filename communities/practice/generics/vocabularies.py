# -*- coding: utf-8 -*-
from zope.schema.vocabulary import SimpleTerm
from zope.schema.vocabulary import SimpleVocabulary


AVAILABLE_FORMS_VOCABULARY = [
    ('BFCheckList', u'Checklist'),
    ('BFGoodPracticeReport', u'Boas Práticas'),
    ('BFVisitAcknowledge', u'Relato de Visita'),
    ('CoPCaseDescription', u'Descrição de Casos'),
    ('CoPTutors', u'Acompanhamento de Tutores'),
]


AVAILABLE_COLORS = [
    ('gray', u'Cinza'),
    ('blue', u'Azul'),
    ('red', u'Vermelho'),
    ('brown', u'Marrom'),
    ('green', u'Verde'),
    ('orange', u'Laranja'),
]


def generateVocabulary(values):
    """Recebe uma lista de (value, title)
    e retorna um SimpleVocabulary
    """
    terms = []
    for pair in values:
        terms.append(SimpleTerm(value=pair[0], title=pair[1]))
    return SimpleVocabulary(terms)

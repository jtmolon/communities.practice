Config
    Digestmail - Send asynchronous email
        You must edit generics/asyncmail_config.py
            Set maildir, where will create the log file 'logger.info' and folders to send emails.
            Set SMTP settings.
    To send news you must edit your buildout, adding a clock server that calls viewCoPClockNotify.
        [instance]
        zope-conf-additional =
                <clock-server>
                method /portal/viewCoPClockNotify
                period 86400
                user admin
                password admin
                host 192.168.0.27:8080
                </clock-server>

IMPORTANT: using SASS v3.3 and Compass v1.0

Install compass using

apt-get install ruby
apt-get install ruby-dev
gem install compass -v 1.0.0.alpha.20
gem install bootstrap-sass -v 3.1.1.1
gem install compass-core -v 1.0.0.alpha.20
gem install compass-import-once -v 1.0.4
gem install sass -v 3.3.9


versions:
------
bootstrap-sass (3.1.1.1)
compass (1.0.0.alpha.20, 0.12.6)
compass-core (1.0.0.alpha.20)
compass-import-once (1.0.4)
sass (3.3.9, 3.2.19)

compile:
------
stylesheets: 
$ compass compile
compiled file will be saved at communities/practice/browser/stylesheets/coppla.css

javascripts:
$ node r.js -o build.js
compiled file will be saved at communities/practice/browser/javascripts/coppla.js

Changelog
=========

4.0 (2014-01-08)
----------------
- Created sub communities

3.2 (2014-01-07)
----------------

3.1 (2013-04-15)
----------------
- Changed object views;
- Created paginators for the views;
- Created caching function for member and community identification;
- Created view for member searching;
- Changed member's portlet;
